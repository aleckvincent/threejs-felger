# The builder from node image
FROM node:alpine as builder
MAINTAINER Mahdi EL JAOUHARI "elmahdi.eljaouhari@gmail.com"


RUN apk update && apk add --no-cache make git

# Move our files into directory name "app"
WORKDIR /app
COPY package.json package-lock.json  /app/
RUN npm install @angular/cli -g
RUN cd /app && npm install
RUN npm install
COPY .  /app

RUN cd /app && ng build

# Build a small nginx image with static website
FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/*
#COPY app.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist/Maison-Felger /usr/share/nginx/html
EXPOSE 81
CMD ["nginx", "-g", "daemon off;"]
