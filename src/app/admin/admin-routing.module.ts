import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthentificationGuard} from './_guards/authentification.guard';
import {AdminLayoutComponent} from './utilities/admin-layout/admin-layout.component';

const routes: Routes = [
  {path: '', canActivate: [AuthentificationGuard], component: AdminLayoutComponent,
    loadChildren: () => import('./utilities/utilities.module').then(mod => mod.UtilitiesModule)
  },
  {
    path: 'auth', component: AdminLayoutComponent,
    loadChildren: () => import('./authentication/authentication.module').then(mod => mod.AuthenticationModule)
  },
  {
    path: 'articles', canActivate: [AuthentificationGuard], component: AdminLayoutComponent,
    loadChildren: () => import('./articles/articles.module').then(mod => mod.ArticlesModule)
  },
  {
    path: 'clients', canActivate: [AuthentificationGuard], component: AdminLayoutComponent,
    loadChildren: () => import('./clients/clients.module').then(mod => mod.ClientsModule)
  },
  {
    path: 'modeles', canActivate: [AuthentificationGuard], component: AdminLayoutComponent,
    loadChildren: () => import('./modeles/modeles.module').then(mod => mod.ModelesModule)
  },
  {
    path: 'roles', canActivate: [AuthentificationGuard], component: AdminLayoutComponent,
    loadChildren: () => import('./roles/roles.module').then(mod => mod.RolesModule)
  },
  {
    path: 'matieres',
    canActivate: [AuthentificationGuard],
    component: AdminLayoutComponent,
    loadChildren: () =>
      import('./matieres/matieres.module').then((mod) => mod.MatieresModule),
  },
  {
    path: 'users',
    loadChildren: () => import('./confirm-user/confirm-user.module').then(mod => mod.ConfirmUserModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
