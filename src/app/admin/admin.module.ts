import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {AuthService} from './services/auth.service';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {UtilitiesModule} from './utilities/utilities.module';
import {ArticlesModule} from './articles/articles.module';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ClientsModule} from './clients/clients.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ModelesModule} from './modeles/modeles.module';
import { RolesModule } from './roles/roles.module';
import { ListModeleComponent } from './modeles/containers/list-modele/list-modele.component';
import { MatieresModule } from './matieres/matieres.module';
import {ConfirmUserRoutingModule} from './confirm-user/confirm-user-routing.module';
import {ConfirmUserModule} from './confirm-user/confirm-user.module';

export function tokenGetter(): string | null {
  return AuthService.token;
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UtilitiesModule,
    AdminRoutingModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatMenuModule,
    MatToolbarModule,
    ArticlesModule,
    MatInputModule,
    MatCardModule,
    MatPaginatorModule,
    ClientsModule,
    MatieresModule,
    ModelesModule,
    ConfirmUserModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class AdminModule {}
