import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from './services/auth.service';
import {ArticleService} from './articles/services/article.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private router: Router,
              private articleService: ArticleService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = sessionStorage.getItem('token');

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request).pipe(
      tap(() => !this.authService.redirecting
        , (error: any) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              this.authService.redirect();
            } else if (error.status === 406) {
              this.router.navigate(['admin/auth/not-authorized']);
            } else if ([400, 409].includes(error.status)) {
              return error.error;
            }
          }
        })
    );

    /*      pipe(
      tap(() => !this.authService.redirecting),
      catchError((err, caught) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.authService.redirect();
            } else if (err.status === 406) {
              this.router.navigate(['admin/auth/not-authorized']);
            } else if ([400, 409].includes(err.status)) {
            }
          }
          return caught;
        }
      )
    ); */
  }
}
