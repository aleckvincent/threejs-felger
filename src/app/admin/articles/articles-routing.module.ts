import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FicheArticleComponent} from './containers/fiche-article/fiche-article.component';
import {ListArticleComponent} from './containers/list-article/list-article.component';
import {ArticlesResolver} from './resolvers/articles.resolver';

const routes: Routes = [
  {path: 'fiche-article/new', component: FicheArticleComponent},
  {path: 'fiche-article/:id',
    children: [
      {
        path: 'show',
        component: FicheArticleComponent, resolve: { article: ArticlesResolver }
      },
      {
        path: 'edit',
        component: FicheArticleComponent, resolve: { article: ArticlesResolver }
      },
    ]
  },
  {path: 'list-article', component: ListArticleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
