import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesRoutingModule } from './articles-routing.module';
import { FicheArticleComponent } from './containers/fiche-article/fiche-article.component';
import { TagsComponent } from './components/tags/tags.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { ListArticleComponent } from './containers/list-article/list-article.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import { DeleteConfirmationDailogComponent } from '../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {UtilitiesModule} from '../utilities/utilities.module';
import {EditorModule} from '@tinymce/tinymce-angular';


@NgModule({
  declarations: [
    FicheArticleComponent,
    TagsComponent,
    ListArticleComponent,
    DeleteConfirmationDailogComponent
  ],
    imports: [
        CommonModule,
        ArticlesRoutingModule,
        MatFormFieldModule,
        MatIconModule,
        MatCardModule,
        ReactiveFormsModule,
        MatChipsModule,
        MatOptionModule,
        MatAutocompleteModule,
        MatInputModule,
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatMenuModule,
        FormsModule,
        MatDialogModule,
        MatSelectModule,
        MatTooltipModule,
        UtilitiesModule,
        EditorModule
    ],
  providers: [
  ]
})
export class ArticlesModule { }
