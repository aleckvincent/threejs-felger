import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {TagsService} from '../../services/tags.service';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../../utilities/models/form-error-state-matcher';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]

})
export class TagsComponent implements OnInit {
  @Input() tags !: IkeyValue<string>[];
  @Input() articleForm !: FormGroup;
  @Input() isEdit !: boolean | false;
  @Input() tagsError !: boolean | false;
  @Output() tagAdd = new EventEmitter<IkeyValue<string>>();
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  public filteredtags$: Observable<IkeyValue<string>[]>;
  tagExist: IkeyValue<string>[] = [];

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement> | any;

  constructor(private readonly tagservice: TagsService, private fb: FormBuilder) {
    this.filteredtags$ = this.tagCtrl.valueChanges.pipe(
      startWith(null),
      map((tag: IkeyValue<string> | null) => tag ? this._filter(tag) : []));
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our tag
    if (value.trim()) {
      this.tags.push({uuid: '', label: value.trim()});
      this.tagAdd.emit({uuid: '', label: value.trim()});
      if (this.tags.length > 0) {
        this.articleForm = this.fb.group({
          tags: [this.tags ? this.tags : null, Validators.required]
        });
        this.tagsError = false;
      }
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.tagCtrl.setValue(null);
  }

  remove(tag: IkeyValue<string>): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
    if (this.tags.length === 0) {
      this.articleForm = this.fb.group({
        tags: [this.tags ? this.tags : null, Validators.required]
      });
      this.tagsError = true;
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push({uuid: '', label: event.option.viewValue});
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
    if (this.tags.length > 0) {
      this.articleForm = this.fb.group({
        tags: [this.tags ? this.tags : null, Validators.required]
      });
      this.tagsError = false;
    }
  }

  getTags(value: string): void {
    this.tagservice.getTags(value).subscribe(tag => {
      this.tagExist = tag;
    });
  }

  ngOnInit(): void {
    this.articleForm = this.fb.group({
      tags: [this.tags ? this.tags : null, Validators.required]
    });
    if (!this.isEdit) {
      this.articleForm.disable();
    }
  }

  private _filter(value: any): IkeyValue<string>[] {
    this.getTags(value);
    return this.tagExist.filter(tag => tag.label.toLowerCase().includes(value.toLowerCase()));
  }
}
