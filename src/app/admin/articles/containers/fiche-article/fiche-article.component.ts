import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {ArticleService} from '../../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleDto} from '../../models/article-dto';
import {StatusService} from '../../services/status.service';
import {DeleteConfirmationDailogComponent} from '../../../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../../utilities/models/form-error-state-matcher';
import {DocumentService} from '../../../clients/services/document.service';
import {FileInputComponent} from '../../../utilities/components/file-input/file-input.component';
import {map} from 'rxjs/operators';
import { AuthService } from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-fiche-article',
  templateUrl: './fiche-article.component.html',
  styleUrls: ['./fiche-article.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]
})
export class FicheArticleComponent implements OnInit {
  tags: IkeyValue<string>[] = [];
  public status$!: Observable<Array<IkeyValue<string>>>;
  SelectedStatus!: string;
  FormTitle!: string;
  public articleForm!: FormGroup;
  public article!: ArticleDto;
  public newArticle!: ArticleDto;
  public isEdit!: boolean | false;
  public id!: string;
  public isShow!: boolean | false;
  contentError!: boolean | false;
  tagsError!: boolean | false;
  public errorForm: IkeyValue<string>[] = [];
  placeholder = 'Image à lier';
  accept = 'jpg,png,jpeg';
  image!: File;
  src!: string;
  sizeMax = 5;
  visibility: boolean = false;
  public filename$!: Observable<string>;
  public fileUpload$!: Observable<File>;
  @ViewChild(FileInputComponent) FileInput!: FileInputComponent;

  constructor(private fb: FormBuilder,
              private readonly articleservice: ArticleService,
              private route: ActivatedRoute, private statuservice: StatusService,
              private dialog: MatDialog,
              private router: Router, private toastr: ToastrService,
              private documentService: DocumentService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN';

    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id;
      this.route.data.subscribe(data => {
        this.article = data.article;
      });
      this.tags = this.article.tags;
      this.SelectedStatus = this.article.status.uuid;
      if (this.article.image.toString() !== '') {
        this.getImageInfo(this.article.image.toString());
        this.documentService.getDocumentInfo(this.article.image.toString()).subscribe((data: any) => {
          this.fileUpload$ = this.documentService.getDocument(this.article.image.toString(), data.originalName, data.mimeType);
        });
      }
    }
    this.getStatus();
    this.initForm();
    if (this.id) {
      if (this.route.snapshot.routeConfig?.path === 'edit') {
        this.isEdit = true;
        this.FormTitle = 'Modification';
      } else {
        this.articleForm.disable();
        this.FormTitle = 'Affichage';
        this.isShow = true;
        this.isEdit = false;
      }
    } else {
      this.isEdit = true;
      this.FormTitle = 'Création';
      this.status$.subscribe(value => {
        for (const element of value) {
          if (element.label === 'Published') {
            this.SelectedStatus = element.uuid;
          }
        }
      });
    }
  }

  initForm(): void {
    this.articleForm = this.fb.group({
      title: [this.article ? this.article.title : null, Validators.required],
      slogan: [this.article ? this.article.slogan : null, [Validators.required, Validators.pattern('^[a-zA-Z-_]+$')]],
      content: [this.article ? this.article.content : null, Validators.required],
      description: [this.article ? (this.article.description === 'null' ? '' : this.article.description) : '', Validators.maxLength(255)],
      status: [this.article ? this.article.status.uuid : null, Validators.required]
    });
  }

  getStatus(): void {
    this.status$ = this.statuservice.getStatus();
  }

  getImageInfo(imageUid: string): void {
    this.filename$ = this.documentService.getDocumentInfo(imageUid).pipe(map((data: any) => {
      return data.originalName;
    }));
  }

  deleteMethod(name: string, id: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {message: name}
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.articleservice.deleteArticle(id).subscribe(
          data => {
            this.router.navigate(['/admin/articles/list-article']);
          }
        );
      }
    });
  }

  save(): void {
    this.tagsError = false;
    this.contentError = false;
    const formValue = this.articleForm.value;
    formValue.tags = this.tags;
    const tmp = [];
    for (const val of formValue.tags) {
      tmp.push(val.label);
    }
    if (tmp.length === 0) {
      this.tagsError = true;
    }
    if (!formValue.content) {
      this.contentError = true;
    }
    const formData: any = new FormData();
    formData.append('title', formValue.title);
    formData.append('slogan', formValue.slogan);
    formData.append('content', formValue.content);
    formData.append('description', formValue.description || null);
    formData.append('status', formValue.status);
    formData.append('tags', JSON.stringify(tmp));
    formData.append('image', this.image || '');
    if (this.route.snapshot.routeConfig?.path === 'edit') {
      if (this.articleForm.valid) {
        this.newArticle = formData;
        this.articleservice.updateArticle(this.newArticle, this.id).subscribe(
          success => {
            const url = '/admin/articles/fiche-article/' + this.id + '/show';
            this.router.navigate([url]);
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (let i = 0; i < errors.length; i++) {
                if (errors[i].key === '') {
                  this.toastr.error(errors[i].code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(errors[i].key)) {
                    this.errorForm.push({uuid: errors[i].key, label: errors[i].code});
                  }
                }
              }
            }
          }
        );
      } else {
        (Object as any).values(this.articleForm.controls).forEach((control: { markAsDirty: () => void; }) => {
          control.markAsDirty();
        });
      }
    } else if (this.route.snapshot.routeConfig?.path === 'fiche-article/new') {
      if (this.articleForm.valid && !this.contentError && !this.tagsError) {
        this.newArticle = formData;
        this.articleservice.addArticle(this.newArticle).subscribe(
          success => {
            this.router.navigate(['/admin/articles/list-article']);
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (let i = 0; i < errors.length; i++) {
                if (errors[i].key === '') {
                  this.toastr.error(errors[i].code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(errors[i].key)) {
                    this.errorForm.push({uuid: errors[i].key, label: errors[i].code});
                  }
                }
              }
            }
          }
        );
      } else {
        this.validateAllFormFields(this.articleForm);
      }
    }
  }

  previewMethod(slogan: string): void {
    const url = this.router.serializeUrl(this.router.createUrlTree(['/article', slogan]));
    window.open(url);
  }

  displayFile(image: Array<File>): void {
    this.image = image[0];
  }
  hideError(): void {
    this.errorForm = [];
  }

  validateAllFormFields(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsDirty();
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
