import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {ArticleDto} from '../../models/article-dto';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {DeleteConfirmationDailogComponent} from '../../../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { AuthService } from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.scss']
})
export class ListArticleComponent implements OnInit, AfterViewInit {
  public articles$!: Observable<Array<ArticleDto>>;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  dataSource = new MatTableDataSource<ArticleDto>();
  private articlesData: ArticleDto[] = [];
  visibility: boolean = false;
  constructor(private readonly articleservice: ArticleService, private dialog: MatDialog, private router: Router, private authService: AuthService) {
  }

  getArticles(): void {
    this.articles$ = this.articleservice.getArticles();
  }
  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' ? true : false;
    this.getArticles();
  }
  ngAfterViewInit(): void {
    this.articles$.subscribe(res => {
      this.articlesData = res;
      this.dataSource.data = this.articlesData;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  deleteMethod(id: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Êtes-vous sûr de vouloir supprimer cet article?',
        title: 'Suppression d\'article'
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.articleservice.deleteArticle(id).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }

  showMethod(uuid: string): void {
    this.router.navigate(['/admin/articles/fiche-article', uuid, 'show']);
  }
  editMethod(uuid: string): void {
    this.router.navigate(['/admin/articles/fiche-article', uuid, 'edit']);
  }
}
