import {IkeyValue} from '../../utilities/models/ikey-value';
export interface ArticleDto {
  uuid: string;
  title: string;
  slogan: string;
  content: string;
  description: any;
  image: File;
  status: IkeyValue<string>;
  tags: Array<IkeyValue<string>>;
}
