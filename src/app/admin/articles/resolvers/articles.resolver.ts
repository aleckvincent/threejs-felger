import {Injectable} from '@angular/core';
import {ArticleService} from '../services/article.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {ArticleDto} from '../models/article-dto';

@Injectable({
  providedIn: 'root'
})
export class ArticlesResolver implements Resolve<ArticleDto> {
  constructor(private articleService: ArticleService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ArticleDto> | ArticleDto[] |any {
    const id = route.paramMap.get('id');
    if (!!id) {
      return this.articleService.getArticle(id);
    }
    throwError(new Error('article not found'));
  }
}
