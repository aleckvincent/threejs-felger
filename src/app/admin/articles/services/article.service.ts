import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ArticleDto} from '../models/article-dto';
import {environment} from '../../../../environments/environment';
import {ApiResponse} from '../../utilities/models/api-response';
import {ApiResponseList} from '../../utilities/models/api-response-list';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  public articlesUrl: string;
  public documentUrl: string;

  constructor(protected http: HttpClient) {
    this.articlesUrl = environment.apiUrl + '/articles/';
    this.documentUrl = environment.apiUrl + '/document/';
  }

  public getArticles(): Observable<ArticleDto[]> {
    return this.http.get<ApiResponseList<ArticleDto>>(this.articlesUrl + 'list')
      .pipe(map((res: ApiResponseList<ArticleDto>) => res.data));
  }

  public addArticle(newArticle: ArticleDto): Observable<any> {
    return this.http.post<ArticleDto>(this.articlesUrl + 'new', newArticle);
  }

  public updateArticle(newArticle: ArticleDto, idArticle: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.articlesUrl + idArticle + '/edit?_method=PUT', newArticle);
  }

  public deleteArticle(idArticle: string): Observable<ApiResponse<any>> {
    return this.http.delete<ApiResponse<any>>(this.articlesUrl + idArticle + '/delete');
  }

  public getArticle(idArticle: string): Observable<ArticleDto> {
    return this.http.get<ApiResponse<ArticleDto>>(this.articlesUrl + idArticle + '/show')
      .pipe(map((res: ApiResponse<ArticleDto>) => res.data));
  }

  public getArticleBySlogan(slogan: string): Observable<ArticleDto> {
    return this.http.get<ApiResponse<ArticleDto>>(this.articlesUrl + 'slogan/' + slogan)
      .pipe(map((res: ApiResponse<ArticleDto>) => res.data));
  }

  public getDocument(idDoc: File): Observable<any> {
    return this.http.get<ApiResponse<any>>(this.documentUrl + 'info/' + idDoc).pipe(map((res:
    ApiResponse<any>) => res.data));
  }
}
