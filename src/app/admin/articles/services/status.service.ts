import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IkeyValue} from '../../utilities/models/ikey-value';
import {ApiResponseList} from '../../utilities/models/api-response-list';import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class StatusService {
  public statusUrl: string;

  constructor(protected  http: HttpClient) {
    this.statusUrl = environment.apiUrl + '/status/';
  }
  public getStatus(): Observable<IkeyValue<string>[]> {
    return this.http.get<ApiResponseList<IkeyValue<string>>>(this.statusUrl + 'list')
      .pipe(map((res: ApiResponseList<IkeyValue<string>>) => res.data));
  }
}
