import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IkeyValue} from '../../utilities/models/ikey-value';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {ApiResponseList} from '../../utilities/models/api-response-list';
@Injectable({
  providedIn: 'root'
})
export class TagsService {
  public tagsUrl: string;
  constructor(protected  http: HttpClient) {
    this.tagsUrl = environment.apiUrl + '/tags/';
  }
  public getTags(tag: string): Observable<IkeyValue<string>[]>{
    return this.http.get<ApiResponseList<IkeyValue<string>>>(this.tagsUrl + 'autocomplete/?query=' + tag)
      .pipe(map((res: ApiResponseList<IkeyValue<string>>) => res.data));
  }
}
