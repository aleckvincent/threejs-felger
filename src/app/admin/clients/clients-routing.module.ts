import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListClientComponent} from './containers/list-client/list-client.component';
import {ClientsResolver} from './resolvers/clients.resolver';
import {FicheClientComponent} from './containers/fiche-client/fiche-client.component';

const routes: Routes = [
  {path: 'list-client', component: ListClientComponent},
  {path: 'fiche-client/:id',
    children: [
      {
        path: 'show',
        component: FicheClientComponent, resolve: { client: ClientsResolver }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
