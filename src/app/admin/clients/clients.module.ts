import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { ClientsRoutingModule } from './clients-routing.module';
import {ListClientComponent} from './containers/list-client/list-client.component';
import {MatCardModule} from '@angular/material/card';
import {UtilitiesModule} from '../utilities/utilities.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ClientAddDialogComponent} from './containers/client-add-dialog/client-add-dialog.component';
import {NgxCaptchaModule} from 'ngx-captcha';
import {MatTooltipModule} from '@angular/material/tooltip';
import { NgxMaskModule} from 'ngx-mask';
import { FicheClientComponent } from './containers/fiche-client/fiche-client.component';

@NgModule({
  declarations: [
    ListClientComponent,
    ClientAddDialogComponent,
    FicheClientComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    MatCardModule,
    UtilitiesModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    NgxCaptchaModule,
    FormsModule,
    MatTooltipModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    MatDatepickerModule,
    DatePipe,
    ReactiveFormsModule,
    MatFormFieldModule,
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ]
})
export class ClientsModule { }
