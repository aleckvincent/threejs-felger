import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {ClientService} from '../../services/client.service';
import {ClientDto} from '../../models/client-dto';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {DatePipe} from '@angular/common';
import {FileInputComponent} from '../../../utilities/components/file-input/file-input.component';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../../utilities/models/form-error-state-matcher';
import {DocumentService} from '../../services/document.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {birthDayValidator} from '../../../utilities/directives/birth-day-validator.directive';

@Component({
  selector: 'app-client-add-dialog',
  templateUrl: './client-add-dialog.component.html',
  styleUrls: ['./client-add-dialog.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]
})
export class ClientAddDialogComponent implements OnInit {
  footLeft!: File;
  footRight!: File;
  payloadFile!: any;
  public clientForm!: FormGroup;
  public client!: ClientDto;
  public newClient!: ClientDto;
  public errorForm: IkeyValue<string>[] = [];
  placeholderLeft = 'Scan pied gauche';
  placeholderRight = 'Scan pied droit';
  sizeMax = 10;
  accept = 'stl';
  sitekey: string;
  langage: string;
  footLeftError!: boolean | false;
  footRightError!: boolean | false;
  public footLeftName$!: Observable<string>;
  public footRightName$!: Observable<string>;
  public footLeftUpload$!: Observable<File>;
  public footRightUpload$!: Observable<File>;

  @ViewChild(FileInputComponent) FileInput!: FileInputComponent;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
              private clientService: ClientService, private documentService: DocumentService,
              private router: Router, private toastr: ToastrService,
              private dialogRef: MatDialogRef<ClientAddDialogComponent>,
              private datePipe: DatePipe, public dialog: MatDialog) {
    this.sitekey = '6Lfee6cbAAAAAGix9KTtj7Q4kFUuCijCUDaH3kLC';
    this.langage = 'fr';
  }

  ngOnInit(): void {
    if (this.dialogRef.componentInstance.data.id) {
      const uuid = this.dialogRef.componentInstance.data.id;
      this.clientService.getClient(uuid)
        .subscribe((data) =>
        {
          this.client = data;
          if (this.client.footPrintLeft.toString() !== '') {
            this.getfootLeftName(this.client.footPrintLeft.toString());
          }
          if (this.client.footPrintRight.toString() !== '') {
            this.getfootRightName(this.client.footPrintRight.toString());
          }
          if (this.client.footPrintLeft.toString() !== '') {
            this.documentService.getDocumentInfo(this.client.footPrintLeft.toString()).subscribe((res: any) => {
                 this.footLeftUpload$ = this.documentService.getDocument(
                   this.client.footPrintLeft.toString(), res.originalName, res.mimeType);
              });
          }
          if (this.client.footPrintRight.toString() !== '') {
            this.documentService.getDocumentInfo(this.client.footPrintRight.toString()).subscribe((res: any) => {
                this.footRightUpload$ = this.documentService.getDocument(
                  this.client.footPrintRight.toString(), res.originalName, res.mimeType);
              });
          }
          this.initForm();
        });
    }
    this.initForm();
  }
  displayFootLeft(file: Array<File>): void  {
    this.footLeft = file[0];
  }
  displayFootRight(file: Array<File>): void  {
    this.footRight = file[0];
  }
  getfootLeftName(imageUid: string): void {
    this.footLeftName$ = this.documentService.getDocumentInfo(imageUid).pipe(map((data: any) => {
      return data.originalName;
    }));
  }
  getfootRightName(imageUid: string): void {
    this.footRightName$ = this.documentService.getDocumentInfo(imageUid).pipe(map((data: any) => {
      return data.originalName;
    }));
  }
  prepareDataToAdd(): void {
    const formValue = this.clientForm.value;
    const birthDate = this.datePipe.transform(formValue.birthDate, 'yyyy/MM/dd');
    const ExpDate = this.datePipe.transform(formValue.printExpirationDate, 'yyyy/MM/dd');
    const formData: any = new FormData();
    this.footLeftError = !this.footLeft;
    this.footRightError = !this.footRight;
    formData.append('footPrintLeft', this.footLeft);
    formData.append('footPrintRight', this.footRight);
    formData.append('firstName', formValue.firstName);
    formData.append('lastName', formValue.lastName);
    formData.append('mail', formValue.mail);
    formData.append('phoneNumber', formValue.phoneNumber);
    formData.append('deliveryAdress', formValue.deliveryAdress);
    formData.append('billingAdress', formValue.billingAdress);
    formData.append('birthDate', birthDate || '');
    formData.append('printExpirationDate', ExpDate);
    this.newClient = formData;
  }
  save(): void {
    this.prepareDataToAdd();
    if (this.clientForm.valid) {
      if (this.dialogRef.componentInstance.data.id) {
        const uuid = this.dialogRef.componentInstance.data.id;
        this.clientService.updateClient(this.newClient, uuid).subscribe(
          success => {
            location.reload();
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (let i = 0; i < errors.length; i++) {
                if (errors[i].key === '') {
                  this.toastr.error(errors[i].code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(errors[i].key)) {
                    this.errorForm.push({uuid: errors[i].key, label: errors[i].code});
                  }
                }
              }
            }
          }
        );
      } else {
        this.clientService.addClient(this.newClient).subscribe(
          success => {
            location.reload();
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (let i = 0; i < errors.length; i++) {
                if (errors[i].key === '') {
                  this.toastr.error(errors[i].code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(errors[i].key)) {
                    this.errorForm.push({uuid: errors[i].key, label: errors[i].code});
                  }
                }
              }
            }
          }
        );
      }
    } else {
      (Object as any).values(this.clientForm.controls).forEach((control: { markAsDirty: () => void; }) => {
        control.markAsDirty();
      });
    }
  }

  close(): void {
    this.dialogRef.close();
    location.reload();
  }
  initForm(): void {
    this.clientForm = this.fb.group({
      firstName: [this.client ? this.client.firstName : null, Validators.required],
      lastName: [this.client ? this.client.lastName : null, Validators.required],
      mail: [this.client ? this.client.mail : null, [Validators.required, Validators.pattern('[(a-z,A-Z)0-9._%+-]+@[(a-z,A-Z)0-9.-]+\\.[(a-z,A-Z)]{2,3}$')]],
      phoneNumber: [this.client ? this.client.phoneNumber : null, [Validators.required, Validators.pattern(/^(?:0|\(\+33\)\s?|\+33\s?|0033\s?)[6-7](?:[\.\-\s]?\d\d){4}$/)]],
      billingAdress: [this.client ? (this.client.billingAdress === 'null' ? '' : this.client.billingAdress) : null, ''],
      deliveryAdress: [this.client ? this.client.deliveryAdress : null, Validators.required],
      birthDate: [this.client ? this.client.birthDate : null, [birthDayValidator()]],
      printExpirationDate: [this.client ? this.client.printExpirationDate : null, Validators.required]
    });
  }
}
