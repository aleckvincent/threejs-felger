import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DocumentService} from '../../services/document.service';
import {ClientDto} from '../../models/client-dto';

@Component({
  selector: 'app-fiche-client',
  templateUrl: './fiche-client.component.html',
  styleUrls: ['./fiche-client.component.scss']
})
export class FicheClientComponent implements OnInit {
  public clientForm!: FormGroup;
  public id!: string;
  public client!: ClientDto;
  footLeftName!: string;
  footRightName!: string;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private documentService: DocumentService) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id;
      this.route.data.subscribe(data => {
        this.client = data.client;
      });
      this.documentService.getDocumentInfo(this.client.footPrintLeft.toString()).subscribe((data: any) => {
        this.footLeftName = data.originalName;
        this.initForm();
      });
      this.documentService.getDocumentInfo(this.client.footPrintRight.toString()).subscribe((data: any) => {
        this.footRightName = data.originalName;
        this.initForm();
      });
    }
    this.initForm();
  }
  initForm(): void {
    this.clientForm = this.fb.group({
      firstName: [this.client ? this.client.firstName : null, ''],
      lastName: [this.client ? this.client.lastName : null, ''],
      mail: [this.client ? this.client.mail : null, ''],
      phoneNumber: [this.client ? this.client.phoneNumber : null, ''],
      billingAdress: [this.client ? (this.client.billingAdress === 'null' ? '' : this.client.billingAdress) : null, ''],
      deliveryAdress: [this.client ? this.client.deliveryAdress : null, ''],
      birthDate: [this.client ? this.client.birthDate : null, ''],
      printExpirationDate: [this.client ? this.client.printExpirationDate : null, ''],
      footPrintLeft: [this.client ? this.footLeftName : null, ''],
      footPrintRight: [this.client ? this.footRightName : null, '']
    });
    this.clientForm.disable();
  }
}
