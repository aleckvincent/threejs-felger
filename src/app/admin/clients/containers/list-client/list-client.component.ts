import {Component, OnInit} from '@angular/core';
import {ClientDto} from '../../models/client-dto';
import {Action} from '../../../utilities/models/action';
import {ClientService} from '../../services/client.service';
import {MatDialog} from '@angular/material/dialog';
import {ClientAddDialogComponent} from '../client-add-dialog/client-add-dialog.component';
import {DeleteConfirmationDailogComponent} from '../../../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {Observable} from 'rxjs';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {AuthService} from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.scss']
})
export class ListClientComponent implements OnInit {
  dataSource: Array<ClientDto> = [];
  actionList: Array<Action> = [];
  rowColumns: Array<IkeyValue<string>> = [];
  headerColumns: Array<string> = [];
  public clients$!: Observable<Array<ClientDto>>;
  public client: ClientDto[] = [];
  visibility: boolean = false;
  visibility_create: boolean = false;

  constructor(public dialog: MatDialog, private clientService: ClientService, private datePipe: DatePipe,
              private router: Router, private authService: AuthService) {
  }

  getClients(): void {
    this.clients$ = this.clientService.getClients();
  }

  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' || rule === 'ROLE_MANAGEMENT_CUSTOMER_ORDER' || rule === 'ROLE_PURE_CONSULTATION' ? true : false;
    this.visibility_create = rule !== 'ROLE_PURE_CONSULTATION' ? true : false;
    this.getClients();
    this.headerColumns = ['lastName', 'firstName', 'mail', 'phoneNumber', 'printExpirationDate'];
    this.rowColumns = [
      {
        uuid: 'lastName',
        label: 'Nom'
      },
      {
        uuid: 'firstName',
        label: 'Prénom'
      },
      {
        uuid: 'mail',
        label: 'Email'
      },
      {
        uuid: 'phoneNumber',
        label: 'Téléphone'
      },
      {
        uuid: 'printExpirationDate',
        label: 'Date d\'expiration'
      }];
    this.actionList.push(
      {
        name: 'Consulter',
        method: 'show',
        icon: 'visibility'
      },
      {
        name: 'Modifier',
        method: 'edit',
        icon: 'edit'
      },
      {
        name: 'Supprimer',
        method: 'delete',
        icon: 'delete'
      }
    );
  }

  openAddClientDialog(): void {
    const dialogRef = this.dialog.open(ClientAddDialogComponent, {
      data: {
        mode: 'Add'
      }
    });
  }

  editMethod(uuid: string): void {
    const dialogRef = this.dialog.open(ClientAddDialogComponent, {
      data: {
        mode: 'Edit',
        id: uuid
      }
    });
  }

  deleteMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Êtes-vous sûr de vouloir supprimer ce client?',
        title: 'Suppression du client'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.clientService.deleteClient(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }

  showMethod(uuid: string): void {
    this.router.navigate(['/admin/clients/fiche-client', uuid, 'show']);
  }
}
