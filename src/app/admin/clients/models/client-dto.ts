export interface ClientDto {
  uuid: string;
  firstName: string;
  lastName: string;
  mail: string;
  phoneNumber: string;
  deliveryAdress: string;
  billingAdress: string;
  birthDate: string;
  footPrintLeft: File;
  footPrintRight: File;
  printExpirationDate: string;
  facture: string;
}
