import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, of, throwError} from 'rxjs';
import {ClientDto} from '../models/client-dto';
import {ClientService} from '../services/client.service';

@Injectable({
  providedIn: 'root'
})
export class ClientsResolver implements Resolve<ClientDto> {
  constructor(private clientService: ClientService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ClientDto> | ClientDto[] |any {
    const id = route.paramMap.get('id');
    if (!!id) {
      return this.clientService.getClient(id);
    }
    throwError(new Error('client not found'));
  }
}
