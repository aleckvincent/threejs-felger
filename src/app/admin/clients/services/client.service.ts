import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ApiResponseList} from '../../utilities/models/api-response-list';
import {ApiResponse} from '../../utilities/models/api-response';
import {map} from 'rxjs/operators';
import {ClientDto} from '../models/client-dto';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  public clientsUrl: string;

  constructor(protected http: HttpClient) {
    this.clientsUrl = environment.apiUrl + '/clients/';
  }
  public getClients(): Observable<ClientDto[]> {
    return this.http.get<ApiResponseList<ClientDto>>(this.clientsUrl + 'list')
      .pipe(map((res: ApiResponseList<ClientDto>) => res.data));
  }
  public getClient(idClient: string): Observable<ClientDto> {
    return this.http.get<ApiResponse<ClientDto>>(this.clientsUrl + idClient + '/show')
      .pipe(map((res: ApiResponse<ClientDto>) => res.data));
  }
  public addClient(newClient: ClientDto): Observable<any> {
    return this.http.post<ClientDto>(this.clientsUrl + 'new', newClient);
  }
  public updateClient(newClient: ClientDto, idClient: string): Observable<ApiResponse<ClientDto>> {
    return this.http.post<ApiResponse<ClientDto>>(this.clientsUrl + idClient + '/edit?_method=PUT', newClient);
  }
  public deleteClient(idClient: string): Observable<ApiResponse<any>> {
    return this.http.delete<ApiResponse<any>>(this.clientsUrl + idClient + '/delete');
  }
}
