import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../utilities/models/api-response';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  public documentUrl: string;
  public documents: Array<File> = [];
  constructor(protected http: HttpClient) {
    this.documentUrl = environment.apiUrl + '/document/';
  }
  public getDocument(idDocument: string, name: string, typeFile: string): Observable<File> {
    return this.http.get<File>(this.documentUrl + idDocument, {responseType: 'arraybuffer' as 'json'}).pipe(
      map((response) => {
        const blob = new Blob([response]);
        const file = new File([blob], name, {type: typeFile});
        return file;
      })
    );
  }

  public getDocumentInfo(idDocument: string): Observable<any> {
    return this.http.get<ApiResponse<any>>(this.documentUrl + idDocument + '/info')
      .pipe(map((res: ApiResponse<any>) => res.data));
  }
  public getDocumentByInfo(idDocument: string): Observable<any> {
    return this.http.get<ApiResponse<any>>(this.documentUrl + idDocument + '/info').pipe(map((res: ApiResponse<any>) => {
      return this.http.get<File>(this.documentUrl + idDocument, {responseType: 'arraybuffer' as 'json'}).pipe(
        map((result: any) => {
          const blob = new Blob([result]);
          const file = new File([blob], res.data.originalName, {type: res.data.mimeType});
          return file;
        }));
    }));
  }
}
