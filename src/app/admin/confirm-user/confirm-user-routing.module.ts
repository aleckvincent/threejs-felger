import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ConfirmUserComponent} from './containers/confirm-user/confirm-user.component';
import {ConfirmUserResolver} from './resolvers/confirm-user.resolver';

const routes: Routes = [
  { path: 'confirm-user/:token',
    component: ConfirmUserComponent,
    resolve: { confirmUser: ConfirmUserResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmUserRoutingModule { }
