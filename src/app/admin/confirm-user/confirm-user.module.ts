import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmUserRoutingModule } from './confirm-user-routing.module';
import { ConfirmUserComponent } from './containers/confirm-user/confirm-user.component';
import {MatCardModule} from '@angular/material/card';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ConfirmUserComponent,
  ],
  imports: [
    CommonModule,
    ConfirmUserRoutingModule,
    MatCardModule,
    AngularSvgIconModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
  ]
})
export class ConfirmUserModule { }
