import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../../utilities/models/form-error-state-matcher';
import {ConfirmedValidator} from '../../directives/check-passwords-validator.directive';
import {ConfirmUserService} from '../../services/confirm-user.service';
import {SnackBarService} from '../../../services/snack-bar.service';

@Component({
  selector: 'app-confirm-user',
  templateUrl: './confirm-user.component.html',
  styleUrls: ['./confirm-user.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]
})
export class ConfirmUserComponent implements OnInit {
  public email!: string;
  public userForm!: FormGroup;
  public token!: string;
  public loading!: boolean;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private confirmUserService: ConfirmUserService,
    private router: Router,
    private notificationService: SnackBarService) { }

  ngOnInit(): void {
    this.token = this.route.snapshot.params.token;
    this.route.data.subscribe(data => {
      this.email = data.confirmUser.email;
    });
    this.initForm();
  }

  initForm(): void {
    this.userForm = this.fb.group({
      email: new FormControl({value: this.email, disabled: true}),
      password: ['', [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
      confirmPassword: ['', Validators.required]
    },  {validator: ConfirmedValidator('password', 'confirmPassword')});
  }
  confirmUser(): void {
    if (this.userForm.valid){
      this.loading = true;
      this.confirmUserService.confirmUser(this.token, this.userForm.get('password')?.value)
        .subscribe(data => {
          this.router.navigate(['/admin']);
          this.notificationService.notification$.next('Le compte a été confirmé avec succès.');
        });
    }
  }
}
