import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {ConfirmUserService} from '../services/confirm-user.service';

@Injectable({
  providedIn: 'root'
})
export class ConfirmUserResolver implements Resolve<string> {
  constructor(private confirmUserService: ConfirmUserService, private router: Router) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    const token = route.paramMap.get('token');
    if (!!token){
      return this.confirmUserService.getUnconfirmedUser(token);
    }
    this.router.navigate(['/admin']);
    return EMPTY;
  }
}
