import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {ApiResponse} from '../../utilities/models/api-response';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';
import {SnackBarService} from '../../services/snack-bar.service';

@Injectable({
  providedIn: 'root'
})
export class ConfirmUserService {
  private confirmUserUrl: string;
  constructor(protected http: HttpClient, protected router: Router, private notificationService: SnackBarService) {
    this.confirmUserUrl = `${environment.apiUrl}/users/confirmation`;
  }
  public getUnconfirmedUser(token: string): Observable<string> {
    return this.http.get<ApiResponse<string>>(`${this.confirmUserUrl}/${token}/get`)
      .pipe(map((res: ApiResponse<string>) => res.data), catchError(err => {
        this.router.navigate(['/admin']);
        this.notificationService.notificationError$.next('Le lien d\'activation est expiré.');
        return EMPTY;
      }));
  }
  public confirmUser(token: string, password: string): Observable <any> {
    return this.http.put(`${this.confirmUserUrl}/${token}/confirm`, {password});
  }
}
