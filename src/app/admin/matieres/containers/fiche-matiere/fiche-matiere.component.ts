import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatiereDto} from '../../models/matiere-dto';
import {ActivatedRoute, Router} from '@angular/router';
import {DocumentService} from '../../../clients/services/document.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-fiche-matiere',
  templateUrl: './fiche-matiere.component.html',
  styleUrls: ['./fiche-matiere.component.scss']
})
export class FicheMatiereComponent implements OnInit {
  public matiereForm!: FormGroup;
  public matiere!: MatiereDto;
  public id!: string;
  imageSrc: any;
  fileName!: string | null;
  checked = '0';
  modelesList: any;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private documentService: DocumentService, private router: Router,
              private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.route.data.subscribe(data => {
      this.matiere = data.matiere;
      this.modelesList = this.matiere.modeles;
    });
    this.documentService.getDocumentInfo(this.matiere.imagePath.toString()).subscribe((data: any) => {
      this.fileName = data.originalName;
      this.documentService.getDocument(this.matiere.imagePath.toString(), data.originalName, data.mimeType).subscribe((res) => {
        const reader = new FileReader();
        reader.readAsDataURL(res);
        reader.onload = (e: any) => {
          this.imageSrc = e.target.result;
        };
      });
      this.initForm();
    });
    this.initForm();
  }
  initForm(): void {
    this.matiereForm = this.fb.group({
      nature:  [this.matiere.nature],
      color: [this.matiere.color],
      caracteristiques: [
        this.matiere.caracteristiques
      ],
      paysOrigin: [
        this.matiere.paysOrigin
      ],
      rupture: [this.matiere.rupture
          ? (this.checked = '1')
          : (this.checked = '0')
      ],
      dateRepriseStock: [this.datePipe.transform(new Date(this.matiere.dateRepriseStock), 'dd-MM-yyy')],
      price: [this.matiere.price],
      imagePath: [this.fileName]
    });
    this.matiereForm.disable();
  }
  showModele(uuid: string): void {
    this.router.navigate(['/admin/modeles/fiche-modele', uuid, 'show']);
  }
}
