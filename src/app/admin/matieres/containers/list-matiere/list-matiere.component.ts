import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Action } from 'src/app/admin/utilities/models/action';
import { IkeyValue } from 'src/app/admin/utilities/models/ikey-value';
import { MatiereDto } from '../../models/matiere-dto';
import { MatiereService } from '../../services/matiere.service';
import { MatiereAddDialogComponent } from '../matiere-add-dialog/matiere-add-dialog.component';
import {DeleteConfirmationDailogComponent} from '../../../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-list-matiere',
  templateUrl: './list-matiere.component.html',
  styleUrls: ['./list-matiere.component.scss'],
})
export class ListMatiereComponent implements OnInit {
  public matiere$!: Observable<Array<MatiereDto>>;
  actionList: Array<Action> = [];
  headerColumns: Array<string> = [];
  rowColumns: Array<IkeyValue<string>> = [];
  visibility: boolean = false;
  visibilityCreate: boolean = false;
  constructor(
    public dialog: MatDialog,
    private matiereService: MatiereService,
    private router: Router,
    private authService: AuthService) {}

  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' || rule === 'ROLE_MANAGEMENT_STOCK' || rule === 'ROLE_PURE_CONSULTATION';
    this.visibilityCreate = rule !== 'ROLE_PURE_CONSULTATION';
    this.getMatieres();
    this.headerColumns = [
      'nature',
      'color',
      'paysOrigin',
      'dateRepriseStock',
      'rupture',
      'price',
    ];
    this.rowColumns = [
      {
        uuid: 'nature',
        label: 'Nature',
      },
      {
        uuid: 'color',
        label: 'Couleur',
      },
      {
        uuid: 'paysOrigin',
        label: 'Pays d \'origine',
      },
      {
        uuid: 'dateRepriseStock',
        label: 'Date reprise du stock',
      },
      {
        uuid: 'rupture',
        label: 'En rupture',
      },
      {
        uuid: 'price',
        label: 'Prix',
      },
    ];
    this.actionList.push(
      {
        name: 'Consulter',
        method: 'show',
        icon: 'visibility'
      },
      {
        name: 'Modifier',
        method: 'edit',
        icon: 'edit'
      },
      {
        name: 'Supprimer',
        method: 'delete',
        icon: 'delete'
      }
    );
  }

  openAddModeleDialog(): void {
     this.dialog.open(MatiereAddDialogComponent, {
      data: {
        mode: 'Add',
      },
    });
  }

  getMatieres(): void {
    this.matiere$ = this.matiereService.getMatieres();
  }

  deleteMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Êtes-vous sûr de vouloir supprimer cette matière?',
        title: 'Suppression de la matière'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.matiereService.deleteMatiere(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }

  editMethod(uuid: string): void {
    const dialogRef = this.dialog.open(MatiereAddDialogComponent, {
      data: {
        mode: 'Edit',
        id: uuid
      }
    });
  }

  showMethod(uuid: string): void {
    this.router.navigate(['/admin/matieres/fiche-matiere', uuid, 'show']);
  }
}
