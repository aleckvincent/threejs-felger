import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher, ThemePalette } from '@angular/material/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {forkJoin, Observable} from 'rxjs';
import { DocumentService } from 'src/app/admin/clients/services/document.service';
import { ModeleDto } from 'src/app/admin/modeles/models/modele-dto';
import { ModeleService } from 'src/app/admin/modeles/services/modele.service';
import { FileInputComponent } from 'src/app/admin/utilities/components/file-input/file-input.component';
import { FormErrorStateMatcher } from 'src/app/admin/utilities/models/form-error-state-matcher';
import { IkeyValue } from 'src/app/admin/utilities/models/ikey-value';
import { MatiereDto } from '../../models/matiere-dto';
import { MatiereService } from '../../services/matiere.service';
import {map} from 'rxjs/operators';
import {element} from 'protractor';

@Component({
  selector: 'app-matiere-add-dialog',
  templateUrl: './matiere-add-dialog.component.html',
  styleUrls: ['./matiere-add-dialog.component.scss'],
  providers: [
    {
      provide: ErrorStateMatcher,
      useClass: FormErrorStateMatcher,
    },
  ],
})
export class MatiereAddDialogComponent implements OnInit {
  imageCuir: File[] = [];
  public matiereForm!: FormGroup;
  public matiere!: MatiereDto;
  public newMatiere!: MatiereDto;
  public errorForm: IkeyValue<string>[] = [];
  imagePlaceholder = 'Photo du cuir';
  sizeMax = 5;
  accept = '.jpg,png,jpeg';
  checked = '0';
  color: ThemePalette = 'primary';
  imageCuirError!: boolean | false;
  public imageCuirName$!: Observable<string>;
  public imageCuirFile$!: Observable<File>;
  imagesCuir: Array<Observable<string>> = [];
  imagesCuirBinary: Array<Observable<File>> = [];
  public models$!: Observable<Array<ModeleDto>>;
  public selected: string[] = [];
  today = new Date();
  @ViewChild(FileInputComponent) FileInput!: FileInputComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private matiereService: MatiereService,
    private documentService: DocumentService,
    private router: Router,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<MatiereAddDialogComponent>,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    private modelService: ModeleService
  ) {}

  ngOnInit(): void {
    if (this.dialogRef.componentInstance.data.id) {
      const uuid = this.dialogRef.componentInstance.data.id;
      this.matiereService.getMatiere(uuid)
        .subscribe((data: MatiereDto) =>
        {
          this.matiere = data;
          if (this.matiere.modeles.length > 0 ) {
            this.matiere.modeles.forEach(modele => {
              this.selected.push(modele.uuid);
            });
          }
          if (this.matiere.imagePath.toString() !== '') {
            this.imagesCuir.push(this.documentService.getDocumentInfo(
              this.matiere.imagePath.toString()).pipe(map((res: any) => res.originalName)));
            this.imagesCuirBinary.push(this.documentService.getDocumentByInfo(
              this.matiere.imagePath.toString()).pipe(map((result: any) => result)));
          }
          this.imageCuirName$ = forkJoin(...this.imagesCuir);
          this.imageCuirFile$ = forkJoin(...this.imagesCuirBinary);
          this.initForm();
        });
    }
    this.initForm();
    this.getModeles();
  }

  initForm(): void {
    this.matiereForm = this.fb.group({
      nature: [this.matiere ? this.matiere.nature : null, Validators.required],
      color: [this.matiere ? this.matiere.color : null, Validators.required],
      caracteristiques: [
        this.matiere ? this.matiere.caracteristiques : null,
        Validators.required,
      ],
      paysOrigin: [
        this.matiere ? this.matiere.paysOrigin : null,
        Validators.required,
      ],
      modeles: [this.matiere ? this.selected : [], Validators.required],
      rupture: [
        this.matiere
          ? this.matiere.rupture
            ? (this.checked = '1')
            : (this.checked = '0')
          : (this.checked = '0'),
        Validators.required,
      ],
      dateRepriseStock: [this.matiere ? this.matiere.dateRepriseStock : null],
      price: [this.matiere ? this.matiere.price : null],
    });
  }

  clearDateRepriseStock(): void {
    this.matiereForm.get('dateRepriseStock')?.setValue(null);
  }

  close(): void {
    this.dialogRef.close();
  }

  displayImageCuir(file: Array<File>): void {
    this.imageCuir = file;
  }

  changed(e: any): void {
    if (e.source.value === '1') {
      this.checked = '1';
      this.matiereForm
        .get('dateRepriseStock')
        ?.setValidators(Validators.required);
      this.matiereForm.get('price')?.setValidators(Validators.required);
    } else {
      this.checked = '0';
      this.matiereForm.get('dateRepriseStock')?.clearValidators();
      this.matiereForm.get('price')?.clearValidators();
    }
  }

  prepareDataToAdd(): void {
    const formValue = this.matiereForm.value;
    const formData: any = new FormData();
    const dateReprise = this.datePipe.transform(
      formValue.dateRepriseStock,
      'yyyy/MM/dd'
    );
    for (const modele of formValue.modeles){
      formData.append('modeles[]', modele);
    }
    formData.append('nature', formValue.nature);
    formData.append('color', formValue.color);
    formData.append('caracteristiques', formValue.caracteristiques);
    formData.append('paysOrigin', formValue.paysOrigin);
    formData.append('imageCuir', this.imageCuir[0]);
    formData.append('dateRepriseStock', dateReprise);
    formData.append('rupture', this.checked === '1');
    formData.append('price', formValue.price);
    this.newMatiere = formData;
  }

  save(): void {
    this.imageCuirError = this.imageCuir.length === 0;
    if (this.matiereForm.valid && !this.imageCuirError) {
      this.prepareDataToAdd();
      if (this.dialogRef.componentInstance.data.id) {
        const uuid = this.dialogRef.componentInstance.data.id;
        this.matiereService.updateMatiere(this.newMatiere, uuid).subscribe (
          (success) => {
            location.reload();
          },
          (error) => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (const err of errors) {
                if (err.key === '') {
                  this.toastr.error(err.code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(err.key)) {
                    this.errorForm.push({ uuid: err.key, label: err.code });
                  }
                }
              }
            }
          }
        );
      } else {
        this.matiereService.addMatiere(this.newMatiere).subscribe(
          (success) => {
            location.reload();
          },
          (error) => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (const err of errors) {
                if (err.key === '') {
                  this.toastr.error(err.code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(err.key)) {
                    this.errorForm.push({ uuid: err.key, label: err.code });
                  }
                }
              }
            }
          }
        );
      }
    } else {
      (Object as any)
        .values(this.matiereForm.controls)
        .forEach((control: { markAsDirty: () => void }) => {
          control.markAsDirty();
        });
    }
  }

  getModeles(): void {
    this.models$ = this.modelService.getModels();
  }
}
