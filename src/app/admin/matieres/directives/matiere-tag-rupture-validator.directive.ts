import { Directive } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function matiereTagRuptureValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const rupture = control.get('rupture')?.value;
    if (rupture) {
      return {
        dateRepriseStockInvalid: true,
        priceInvalid: true,
      };
    }
    return null;
  };
}

@Directive({
  selector: '[appMatiereTagRuptureValidator]',
})
export class MatiereTagRuptureValidatorDirective {
  constructor() {}
}
