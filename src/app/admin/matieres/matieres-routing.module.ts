import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListMatiereComponent } from './containers/list-matiere/list-matiere.component';
import {FicheMatiereComponent} from './containers/fiche-matiere/fiche-matiere.component';
import {MatiereResolver} from './resolvers/matiere.resolver';

const routes: Routes = [
  { path: 'list-matieres', component: ListMatiereComponent },
  { path: 'fiche-matiere/:id',
    children: [
      {
        path: 'show',
        component: FicheMatiereComponent, resolve: { matiere: MatiereResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MatieresRoutingModule {}
