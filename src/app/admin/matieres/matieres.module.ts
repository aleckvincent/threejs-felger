import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UtilitiesModule } from '../utilities/utilities.module';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ModelesRoutingModule } from '../modeles/modeles-routing.module';
import { ListMatiereComponent } from './containers/list-matiere/list-matiere.component';
import { MatieresRoutingModule } from './matieres-routing.module';
import { MatiereAddDialogComponent } from './containers/matiere-add-dialog/matiere-add-dialog.component';
import { MatiereTagRuptureValidatorDirective } from './directives/matiere-tag-rupture-validator.directive';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { FicheMatiereComponent } from './containers/fiche-matiere/fiche-matiere.component';
import {MatListModule} from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [
    ListMatiereComponent,
    MatiereAddDialogComponent,
    MatiereTagRuptureValidatorDirective,
    FicheMatiereComponent,
  ],
  imports: [
    CommonModule,
    MatieresRoutingModule,
    CommonModule,
    ModelesRoutingModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    UtilitiesModule,
    MatSelectModule,
    MatCardModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatSelectCountryModule,
    HttpClientModule,
    MatDatepickerModule,
    MatListModule,
    MatRadioModule
  ],
})
export class MatieresModule {}
