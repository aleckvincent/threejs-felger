import { IkeyValue } from '../../utilities/models/ikey-value';

export interface MatiereDto {
  nature: string;
  color: string;
  caracteristiques: string;
  paysOrigin: string;
  imagePath: File;
  dateRepriseStock: string;
  rupture: boolean;
  price: number;
  modeles: Array<IkeyValue<string>>;
}

