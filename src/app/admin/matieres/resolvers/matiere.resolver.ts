import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot, Router
} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {MatiereService} from '../services/matiere.service';
import {MatiereDto} from '../models/matiere-dto';

@Injectable({
  providedIn: 'root'
})
export class MatiereResolver implements Resolve<MatiereDto> {
  constructor(private matiereService: MatiereService, private router: Router) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MatiereDto> {
    const id = route.paramMap.get('id');
    if (!!id){
      return this.matiereService.getMatiere(id);
    }
    this.router.navigate(['/list-matieres']);
    return EMPTY;
  }
}
