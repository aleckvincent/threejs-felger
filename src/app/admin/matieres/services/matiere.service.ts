import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../../utilities/models/api-response';
import { ApiResponseList } from '../../utilities/models/api-response-list';
import { MatiereDto } from '../models/matiere-dto';
import {ClientDto} from '../../clients/models/client-dto';

@Injectable({
  providedIn: 'root',
})
export class MatiereService {
  private matiereUrl: string;
  constructor(protected http: HttpClient) {
    this.matiereUrl = `${environment.apiUrl}/matieres/`;
  }

  public getMatieres(): Observable<MatiereDto[]> {
    return this.http
      .get<ApiResponseList<MatiereDto>>(`${this.matiereUrl}\list`)
      .pipe(map((res: ApiResponseList<MatiereDto>) => res.data));
  }

  public addMatiere(newMatiere: MatiereDto): Observable<any> {
    return this.http.post<MatiereDto>(`${this.matiereUrl}new`, newMatiere);
  }

  public updateMatiere(
    newMatiere: MatiereDto,
    idMatiere: string
  ): Observable<ApiResponse<MatiereDto>> {
    return this.http.post<ApiResponse<MatiereDto>>(
      `${this.matiereUrl}${idMatiere}/edit?_method=PUT'`,
      newMatiere
    );
  }

  public deleteMatiere(idMatiere: string): Observable<ApiResponse<any>> {
    return this.http.delete<ApiResponse<any>>(`${this.matiereUrl + idMatiere}/delete`);
  }

  public getMatiere(idMatiere: string): Observable<MatiereDto> {
    return this.http.get<ApiResponse<MatiereDto>>(`${this.matiereUrl}${idMatiere}/show`)
      .pipe(map((res: ApiResponse<MatiereDto>) => res.data));
  }
}
