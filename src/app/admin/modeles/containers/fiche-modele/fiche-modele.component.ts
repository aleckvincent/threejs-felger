import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ModeleDto} from '../../models/modele-dto';
import {DocumentService} from '../../../clients/services/document.service';

@Component({
  selector: 'app-fiche-modele',
  templateUrl: './fiche-modele.component.html',
  styleUrls: ['./fiche-modele.component.scss']
})
export class FicheModeleComponent implements OnInit {
  public modeleForm!: FormGroup;
  public modele!: ModeleDto;
  public id!: string;
  imageSrc: any [] = [];
  filenames!: string | null;
  checked = 'false';
  constructor(private route: ActivatedRoute, private fb: FormBuilder, private documentService: DocumentService) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id;
      this.route.data.subscribe(data => {
        this.modele = data.modele;
      });
      if (this.modele.file3D.length > 0) {
        this.modele.file3D.forEach((item) => {
          this.documentService.getDocumentInfo(item.toString()).subscribe((data: any) => {
            this.filenames = this.filenames ? this.filenames + data.originalName + ',' : data.originalName + ',';
            this.documentService.getDocument(item.toString(), data.originalName, data.mimeType).subscribe((res) =>
            {
              const reader = new FileReader();
              reader.readAsDataURL(res);
              reader.onload = (e: any) => {
                this.imageSrc.push(e.target.result);
              };
            });
            this.initForm();
          });
        });
      }
    }
    this.initForm();
  }
  initForm(): void {
    this.modeleForm = this.fb.group({
      name: [this.modele ? this.modele.name : null, Validators.required],
      reference: [this.modele ? this.modele.reference : null, Validators.required],
      characteristic: [this.modele ? (this.modele.characteristic === 'null' ? '' : this.modele.characteristic) : ''],
      fabricDuration: [this.modele ? this.modele.fabricDuration : null, ''],
      price: [this.modele ? this.modele.price : null, Validators.required],
      visibility: [this.modele ? (this.modele.visibility === true ? this.checked = 'true' : this.checked = 'false') : null, ''],
      file3D: [this.modele ? this.filenames : null, '']
    });
    this.modeleForm.disable();
  }
}
