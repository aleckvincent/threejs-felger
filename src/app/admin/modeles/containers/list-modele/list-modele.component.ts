import { Component, OnInit } from '@angular/core';
import {ModeleDto} from '../../models/modele-dto';
import {Action} from '../../../utilities/models/action';
import {MatDialog} from '@angular/material/dialog';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {ModeleAddDialogComponent} from '../modele-add-dialog/modele-add-dialog.component';
import {Observable} from 'rxjs';
import {DeleteConfirmationDailogComponent} from '../../../utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import {ModeleService} from '../../services/modele.service';
import {Router} from '@angular/router';
import { AuthService } from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-list-modele',
  templateUrl: './list-modele.component.html',
  styleUrls: ['./list-modele.component.scss']
})
export class ListModeleComponent implements OnInit {
  public modele$!: Observable<Array<ModeleDto>>;
  dataSource: Array<ModeleDto> = [];
  actionList: Array<Action> = [];
  headerColumns: Array<string> = [];
  rowColumns: Array<IkeyValue<string>> = [];
  visibility: boolean = false;
  visibility_create: boolean = false;
  constructor(public dialog: MatDialog, private modeleService: ModeleService, private router: Router, private authService: AuthService) { }
  getModeles(): void {
    this.modele$ = this.modeleService.getModels();
  }
  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' || rule === 'ROLE_MANAGEMENT_STOCK' || rule === 'ROLE_PURE_CONSULTATION' ? true : false;
    this.visibility_create = rule !== 'ROLE_PURE_CONSULTATION' ? true : false;
    this.getModeles();
    this.headerColumns = ['name', 'reference', 'characteristic', 'fabricDuration', 'price', 'visibility'];
    this.rowColumns = [
      {
        uuid: 'name',
        label: 'Nom du modèle'
      },
      {
        uuid: 'reference',
        label: 'Référence'
      },
      {
        uuid: 'characteristic',
        label: 'Caractéristiques'
      },
      {
        uuid: 'fabricDuration',
        label: 'Durée de fabrication'
      },
      {
        uuid: 'price',
        label: 'Prix'
      },
      {
        uuid: 'visibility',
        label: 'Visibilité'
      }];
    this.actionList.push(
      {
        name: 'Consulter',
        method: 'show',
        icon: 'visibility'
      },
      {
        name: 'Modifier',
        method: 'edit',
        icon: 'edit'
      },
      {
        name: 'Activer',
        method: 'activate',
        icon: 'check',
      },
      {
        name: 'Désactiver',
        method: 'deactivate',
        icon: 'check',
      },
      {
        name: 'Supprimer',
        method: 'delete',
        icon: 'delete',
      }
    );
  }
  openAddModeleDialog(): void{
    const dialogRef = this.dialog.open(ModeleAddDialogComponent, {
      data: {
        mode: 'Add'
      }
    });
  }
  activateMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Vous êtes sûr que vous voulez activer ce modèle?',
        title: 'Activation du modèle'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.modeleService.activateModel(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }
  deactivateMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Vous êtes sûr que vous voulez Désactiver ce modèle?',
        title: 'Désactivation du modèle'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.modeleService.deactivateModel(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }
  deleteMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Vous êtes sûr que vous voulez supprimer ce modèle?',
        title: 'Suppression du modèle'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.modeleService.deleteModel(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }
  editMethod(uuid: string): void {
    const dialogRef = this.dialog.open(ModeleAddDialogComponent, {
      data: {
        mode: 'Edit',
        id: uuid
      }
    });
  }
  showMethod(uuid: string): void {
    this.router.navigate(['/admin/modeles/fiche-modele', uuid, 'show']);
  }
}
