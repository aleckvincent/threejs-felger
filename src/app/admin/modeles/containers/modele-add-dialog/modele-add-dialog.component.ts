import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FileInputComponent} from '../../../utilities/components/file-input/file-input.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {IkeyValue} from '../../../utilities/models/ikey-value';
import {ModeleDto} from '../../models/modele-dto';
import {ErrorStateMatcher, ThemePalette} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../../utilities/models/form-error-state-matcher';
import {ModeleService} from '../../services/modele.service';
import {DocumentService} from '../../../clients/services/document.service';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-modele-add-dialog',
  templateUrl: './modele-add-dialog.component.html',
  styleUrls: ['./modele-add-dialog.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]
})
export class ModeleAddDialogComponent implements OnInit {
  files: File[] = [];
  public modeleForm!: FormGroup;
  public modele!: ModeleDto;
  public newModele!: ModeleDto;
  public errorForm: IkeyValue<string>[] = [];
  placeholder = 'Télécharger le modèle';
  accept = 'jpg,jpeg,png';
  color: ThemePalette = 'primary';
  checked = 'true';
  duration: number [] = [];
  imageSrc: any [] = [];
  sizeMax = 5;
  public fileNames$!: Observable<string>;
  public filesUpload$!: Observable<File>;
  filesName: Array<Observable<string>> = [];
  filesBinary: Array<Observable<File>> = [];
  @ViewChild(FileInputComponent) FileInput!: FileInputComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
              private router: Router, private toastr: ToastrService,
              private dialogRef: MatDialogRef<ModeleAddDialogComponent>,
              private modeleService: ModeleService, private documentService: DocumentService) {
  }

  ngOnInit(): void {
    if (this.dialogRef.componentInstance.data.id) {
      const uuid = this.dialogRef.componentInstance.data.id;
      this.modeleService.getModel(uuid).subscribe((data) => {
        this.modele = data;
        this.modele.file3D.forEach((item) => {
          this.filesName.push(this.documentService.getDocumentInfo(item.toString()).pipe(map((res: any) => res.originalName)));
          this.filesBinary.push(this.documentService.getDocumentByInfo(item.toString()).pipe(map((result: any) => result)));
        });
        this.fileNames$ = forkJoin(...this.filesName);
        this.filesUpload$ = forkJoin(...this.filesBinary);
        this.initForm();
      });
    }
    this.initForm();
    for (let i = 1; i < 60; i++) {
      this.duration.push(i);
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  initForm(): void {
    this.modeleForm = this.fb.group({
      name: [this.modele ? this.modele.name : null, Validators.required],
      reference: [this.modele ? this.modele.reference : null, Validators.required],
      characteristic: [this.modele ? (this.modele.characteristic === 'null' ?
        '' : this.modele.characteristic) : null, Validators.maxLength(255)],
      fabricDuration: [this.modele ? this.modele.fabricDuration : null, ''],
      price: [this.modele ? this.modele.price : null, Validators.required],
      visibility: [this.modele ? (this.modele.visibility ?
        this.checked = 'true' : this.checked = 'false') : this.checked = 'false', Validators.required]
    });
  }

  changed(e: any): void {
    if (e.source.value) {
      this.checked = 'true';
    } else {
      this.checked = 'false';
    }
  }

  displayFile(file: Array<File>): void {
    this.files = file;
  }

  prepareDataToAdd(): void {
    const formValue = this.modeleForm.value;
    const formData: any = new FormData();
    if (this.files.length > 0) {
      for (const ele of this.files) {
        formData.append('file3D[]', ele);
      }
    } else {
      formData.append('file3D', []);
    }
    formData.append('name', formValue.name);
    formData.append('reference', formValue.reference);
    formData.append('characteristic', formValue.characteristic);
    formData.append('fabricDuration', formValue.fabricDuration || 0);
    formData.append('price', formValue.price);
    formData.append('visibility', formValue.visibility);
    this.newModele = formData;
  }

  save(): void {
    this.prepareDataToAdd();
    if (this.modeleForm.valid) {
      if (this.dialogRef.componentInstance.data.id) {
        const uuid = this.dialogRef.componentInstance.data.id;
        this.modeleService.updateModel(this.newModele, uuid).subscribe(success => {
            location.reload();
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (const err of errors) {
                if (err.key === '') {
                  this.toastr.error(err.code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(err.key)) {
                    this.errorForm.push({uuid: err.key, label: err.code});
                  }
                }
              }
            }
          });
      } else {
        this.modeleService.addModel(this.newModele).subscribe(
          success => {
            location.reload();
          },
          error => {
            if (error.status === 400 && error.error.errors) {
              const errors = error.error.errors;
              for (const err of errors) {
                if (err.key === '') {
                  this.toastr.error(err.code);
                  return;
                } else {
                  const existingUuid = this.errorForm.map((obj) => obj.uuid);
                  if (!existingUuid.includes(err.key)) {
                    this.errorForm.push({uuid: err.key, label: err.code});
                  }
                }
              }
            }
          }
        );
      }
    } else {
      (Object as any).values(this.modeleForm.controls).forEach((control: { markAsDirty: () => void; }) => {
        control.markAsDirty();
      });
    }
  }
}
