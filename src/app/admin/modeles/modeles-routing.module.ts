import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListModeleComponent} from './containers/list-modele/list-modele.component';
import {ModelResolver} from './resolvers/modele.resolver';
import {FicheModeleComponent} from './containers/fiche-modele/fiche-modele.component';

const routes: Routes = [
  {path: 'list-modele', component: ListModeleComponent},
  {path: 'fiche-modele/:id',
    children: [
      {
        path: 'show',
        component: FicheModeleComponent, resolve: { modele: ModelResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModelesRoutingModule { }
