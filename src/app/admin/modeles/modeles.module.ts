import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModelesRoutingModule } from './modeles-routing.module';
import {ListModeleComponent} from './containers/list-modele/list-modele.component';
import { ModeleAddDialogComponent } from './containers/modele-add-dialog/modele-add-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {UtilitiesModule} from '../utilities/utilities.module';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FicheModeleComponent} from './containers/fiche-modele/fiche-modele.component';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  declarations: [
    ListModeleComponent,
    ModeleAddDialogComponent,
    FicheModeleComponent
  ],
    imports: [
        CommonModule,
        ModelesRoutingModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        UtilitiesModule,
        MatSelectModule,
        MatCardModule,
        MatPaginatorModule,
        MatIconModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatRadioModule
    ]
})
export class ModelesModule { }
