export interface ModeleDto {
  uuid: string;
  name: string;
  reference: string;
  characteristic: string;
  fabricDuration: number;
  price: number;
  file3D: Array<File>;
  visibility: boolean;
}
