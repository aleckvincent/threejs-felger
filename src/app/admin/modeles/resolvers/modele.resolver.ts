import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {ModeleDto} from '../models/modele-dto';
import {ModeleService} from '../services/modele.service';

@Injectable({
  providedIn: 'root'
})
export class ModelResolver implements Resolve<ModeleDto> {
  constructor(private modelService: ModeleService) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ModeleDto> | ModeleDto[] |any {
    const id = route.paramMap.get('id');
    if (!!id) {
      return this.modelService.getModel(id);
    }
    throwError(new Error('modele not found'));
  }
}
