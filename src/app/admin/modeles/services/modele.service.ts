import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ApiResponse} from '../../utilities/models/api-response';
import {ApiResponseList} from '../../utilities/models/api-response-list';

import {map} from 'rxjs/operators';
import {ModeleDto} from '../models/modele-dto';

@Injectable({
  providedIn: 'root'
})
export class ModeleService {
  public modelesUrl: string;
  constructor(protected http: HttpClient) {
    this.modelesUrl = environment.apiUrl + '/modeles/';
  }
  public getModels(): Observable<ModeleDto[]> {
    return this.http.get<ApiResponseList<ModeleDto>>(this.modelesUrl + 'list')
      .pipe(map((res: ApiResponseList<ModeleDto>) => res.data));
  }
  public getModel(idModel: string): Observable<ModeleDto> {
    return this.http.get<ApiResponse<ModeleDto>>(this.modelesUrl + idModel + '/show')
      .pipe(map((res: ApiResponse<ModeleDto>) => res.data));
  }
  public addModel(newModele: ModeleDto): Observable<any> {
    return this.http.post<ModeleDto>(this.modelesUrl + 'new', newModele);
  }
  public updateModel(newModel: ModeleDto, idModel: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.modelesUrl + idModel + '/edit?_method=PUT', newModel);
  }
  public deleteModel(idModel: string): Observable<ApiResponse<any>> {
    return this.http.delete<ApiResponse<any>>(this.modelesUrl + idModel + '/delete');
  }
  public activateModel(idModel: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.modelesUrl + idModel + '/activate?_method=PATCH', {});
  }
  public deactivateModel(idModel: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.modelesUrl + idModel + '/deactivate?_method=PATCH', {});
  }
}
