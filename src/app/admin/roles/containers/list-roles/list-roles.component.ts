import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DeleteConfirmationDailogComponent } from 'src/app/admin/utilities/components/delete-confirmation-dailog/delete-confirmation-dailog.component';
import { IkeyValue } from 'src/app/admin/utilities/models/ikey-value';
import { RoleDto } from '../../models/role-dto';
import { RolesService } from '../../services/roles.service';
import { RolesAddDialogComponent } from '../roles-add-dialog/roles-add-dialog.component';
import {Action} from '../../../utilities/models/action';
import { AuthService } from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-list-roles',
  templateUrl: './list-roles.component.html',
  styleUrls: ['./list-roles.component.scss']
})
export class ListRolesComponent implements OnInit {
  visibility: boolean = false;
  public role$!: Observable<Array<RoleDto>>;
  dataSource: Array<RoleDto> = [];
  actionList: Array<Action> = [];
  headerColumns: Array<string> = [];
  rowColumns: Array<IkeyValue<string>> = [];
  constructor(public dialog: MatDialog, private roleService: RolesService, private router: Router, private authService: AuthService) { }
  getRoles(): void {
    this.role$ = this.roleService.getRoles();
  }
  ngOnInit(): void {
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' ? true : false;
    this.getRoles();
    this.headerColumns = ['nom', 'prenom', 'username', 'email', 'statut', 'roles'];
    this.rowColumns = [
      {
        uuid: 'nom',
        label: 'Nom'
      },
      {
        uuid: 'prenom',
        label: 'Prénom'
      },
      {
        uuid: 'username',
        label: 'nom d\'utilisateur'
      },
      {
        uuid: 'email',
        label: 'Email'
      },
      {
        uuid: 'statut',
        label: 'Statut'
      },
      {
        uuid: 'roles',
        label: 'Habilitation'
      }];
    this.actionList.push(
      {
        name: 'Activer',
        method: 'activate',
        icon: 'check',
      },
      {
        name: 'Désactiver',
        method: 'deactivate',
        icon: 'check',
      }
    );
  }
  openAddRoleDialog(): void{
    const dialogRef = this.dialog.open(RolesAddDialogComponent, {
      data: {
        mode: 'Add'
      }
    });
  }
  activateMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Êtes-vous sûr de vouloir activer cet utilisateur?',
        title: 'Activation d\'utilisateur'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.roleService.activateRole(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }
  deactivateMethod(uuid: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDailogComponent, {
      data: {
        message: 'Êtes-vous sûr de vouloir désactiver un utilisateur?',
        title: 'Désactivation d\'utilisateur'
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.roleService.deactivateRole(uuid).subscribe(
          data => {
            location.reload();
          }
        );
      }
    });
  }

}
