import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher, ThemePalette } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormErrorStateMatcher } from 'src/app/admin/utilities/models/form-error-state-matcher';
import { IkeyValue } from 'src/app/admin/utilities/models/ikey-value';
import { RoleDto } from '../../models/role-dto';
import { RolesService } from '../../services/roles.service';

@Component({
  selector: 'app-roles-add-dialog',
  templateUrl: './roles-add-dialog.component.html',
  styleUrls: ['./roles-add-dialog.component.scss'],
  providers: [{
    provide: ErrorStateMatcher, useClass: FormErrorStateMatcher
  }]
})
export class RolesAddDialogComponent implements OnInit {
  public userForm!: FormGroup;
  public roles!: RoleDto;
  color: ThemePalette  = 'primary';
  checked = 'true';
  public newRole!: RoleDto;
  public errorForm: IkeyValue<string>[] = [];
  public hide = [true, true];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private router: Router, private toastr: ToastrService,
                private dialogRef: MatDialogRef<RolesAddDialogComponent>, private roleService: RolesService) { }

  ngOnInit(): void {
    this.initForm();
  }
  close(): void {
    this.dialogRef.close();
  }
  initForm(): void {
    this.userForm = this.fb.group({
      nom: [this.roles ? this.roles.nom : null, Validators.required],
      prenom: [this.roles ? this.roles.prenom : null, Validators.required],
      username: [this.roles ? this.roles.username: null, Validators.required],
      email: [this.roles ? this.roles.email : null, [Validators.required, Validators.pattern('[(a-z,A-Z)0-9._%+-]+@[(a-z,A-Z)0-9.-]+\\.[(a-z,A-Z)]{2,3}$')]],
      adresse: [this.roles ? this.roles.adresse : null, Validators.required],
      role: [this.roles ? this.roles.role : null, Validators.required],
      statut: [this.roles ? (this.roles.statut === true ?
        this.checked = 'true' : this.checked = 'false') : this.checked = 'false', Validators.required]
    });
  }
  changed(e: any): void {
    if (e.source.value) {
      this.checked = 'true';
    } else {
      this.checked = 'false';
    }
  }
  prepareDataToAdd(): void {
    const rules = [];
    const formValue = this.userForm.value;
    rules.push(formValue.role);
    const formData: any = new FormData();
    formData.append('nom', formValue.nom);
    formData.append('prenom', formValue.prenom);
    formData.append('email', formValue.email);
    formData.append('username', formValue.username);
    formData.append('adresse', formValue.adresse);
    formData.append('statut', formValue.statut);
    formData.append('role', JSON.stringify(rules));
    this.newRole = formData;
  }
  save(): void {
    this.prepareDataToAdd();
    if (this.userForm.valid) {
      this.roleService.addRole(this.newRole).subscribe(
        success => {
          location.reload();
        },
        error => {
          if (error.status === 400 && error.error.errors) {
            const errors = error.error.errors;
            for (let i = 0; i < errors.length; i++) {
              if (errors[i].key === '') {
                this.toastr.error(errors[i].code);
                return;
              } else {
                const existingUuid = this.errorForm.map((obj) => obj.uuid);
                if (!existingUuid.includes(errors[i].key)) {
                  this.errorForm.push({uuid: errors[i].key, label: errors[i].code});
                }
              }
            }
          }
        }
      );
    } else {
      (Object as any).values(this.userForm.controls).forEach((control: { markAsDirty: () => void; }) => {
        control.markAsDirty();
      });
    }
  }

}
