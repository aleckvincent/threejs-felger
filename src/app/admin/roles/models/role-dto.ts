export interface RoleDto {
  uuid: string;
  username: string;
  password: string;
  nom: string;
  prenom: string;
  adresse: string;
  email: string;
  statut: boolean;
  role: string;
}
