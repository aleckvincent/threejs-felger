import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { ListRolesComponent } from './containers/list-roles/list-roles.component';
import { RolesAddDialogComponent } from './containers/roles-add-dialog/roles-add-dialog.component';
import { UtilitiesModule } from '../utilities/utilities.module';
import {MatCardModule} from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  declarations: [
    ListRolesComponent,
    RolesAddDialogComponent
  ],
    imports: [
        CommonModule,
        RolesRoutingModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        UtilitiesModule,
        MatSelectModule,
        MatCardModule,
        MatPaginatorModule,
        MatIconModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatRadioModule
    ]
})
export class RolesModule { }
