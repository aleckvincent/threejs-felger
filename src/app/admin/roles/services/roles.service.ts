import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import { ApiResponse } from '../../utilities/models/api-response';
import { ApiResponseList } from '../../utilities/models/api-response-list';
import { RoleDto } from '../models/role-dto';
import {MatiereDto} from '../../matieres/models/matiere-dto';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  public rolesUrl: string;
  constructor(protected http: HttpClient) {
    this.rolesUrl = environment.apiUrl + '/users/';
  }
  public addRole(newRole: RoleDto): Observable<any> {
    return this.http.post<RoleDto>(this.rolesUrl + 'new', newRole);
  }
  public getRoles(): Observable<RoleDto[]> {
    return this.http.get<ApiResponseList<RoleDto>>(this.rolesUrl + 'list')
      .pipe(map((res: ApiResponseList<RoleDto>) => res.data));
  }
  public activateRole(idRole: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.rolesUrl + idRole + '/activate?_method=PATCH', {});
  }
  public deactivateRole(idRole: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(this.rolesUrl + idRole + '/deactivate?_method=PATCH', {});
  }
}
