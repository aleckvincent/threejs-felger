import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public redirecting = false;

  constructor(
    protected http: HttpClient,
    private readonly jwtHelperService: JwtHelperService,
    private router: Router,
    private cookieService: CookieService,
    private toastr: ToastrService
  ) {
  }

  static get token(): string | null {
    return sessionStorage.getItem('token');
  }

  static set token(value) {
    if (!value) {
      sessionStorage.removeItem('token');
    } else {
      sessionStorage.setItem('token', value);
    }
  }

  static get username(): string | null {
    return sessionStorage.getItem('username');
  }

  static set username(value) {
    if (!value) {
      sessionStorage.removeItem('username');
    } else {
      sessionStorage.setItem('username', value);
    }
  }

  static get counter(): number | null {
    return Number(sessionStorage.getItem('counter'));
  }

  static set counter(value) {
    sessionStorage.setItem('counter', String(value));
  }

  public login(login: string, password: string, remember?: boolean): void {
    if (!AuthService.counter) {
      AuthService.counter = 1;
    } else {
      AuthService.counter++;
    }
    const form = {
      username: login, password
    };
    this.http.post(`${environment.apiUrl}/auth/login_check`, form).subscribe(
      (res: any) => {
        if (res.data.error) {
          this.toastr.error(res.data.error);
        } else {
          sessionStorage.removeItem('counter');
          AuthService.token = res.token;
          AuthService.username = res.data.fullName;
          if (remember) {
            this.cookieService.set('token', res.token, 600);
          }
          this.router.navigate(['admin']);
        }
      },
      (error) => {
        if (+error.status === 406) {
         this.router.navigate(['admin/auth/not-authorized']);
        } else {
          this.checkCount();
        }
      }
    );
  }


  public checkCount(): void {
    if (AuthService.counter && AuthService.counter > 4) {
       this.router.navigate(['admin/auth/erreur']);
    } else {
      this.toastr.error('Identifiant ou mot de passe incorrect.');
    }
  }

  public logout(): void {
    this.isAuthenticated().pipe(
      tap(auth => {
          if (auth) {
            this.http.get(`${environment.apiUrl}/auth/logout`).pipe(tap(
              () => {
                AuthService.token = null;
                sessionStorage.clear();
                this.toLogin().then();
              }
            )).subscribe(() => this.toastr.info('Utilisateur déconnecté'));
          } else {
            this.toLogin().then();
          }
        }
      )
    ).subscribe();
  }

  public isAuthenticated(): any {
    if (this.cookieService.check('token')) {
      AuthService.token = this.cookieService.get('token');
    }
    return of(!this.jwtHelperService.isTokenExpired()).pipe(
      tap(allow => {
        if (!allow) {
          this.toLogin();
        }
      })
    );
  }

  public redirect(): void {
    if (!this.redirecting) {
      this.redirecting = true;
      this.toLogin().then(() => this.redirecting = false);
    }
  }

  private toLogin(): Promise<boolean> {
    this.cookieService.delete('token');
    AuthService.token = null;
    return this.router.navigate(['admin/auth/login']);
  }

  public readRole() : string {
    const token = this.jwtHelperService.decodeToken(sessionStorage.token);
    return token.roles[0];
  }

}
