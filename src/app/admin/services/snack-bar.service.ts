import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  public notification$: Subject<string> = new Subject();
  public notificationError$: Subject<string> = new Subject();
}
