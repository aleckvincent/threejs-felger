import {Component, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MatSidenav} from '@angular/material/sidenav';
import {Link} from '../models/link';
import {AuthService} from '../../services/auth.service';


@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  roles =  ['ROLE_SUPER_ADMIN', 'ROLE_MANAGEMENT_STOCK', 'ROLE_MANAGEMENT_CUSTOMER_ORDER', 'ROLE_PURE_CONSULTATION'];
  navigationLink: Link[] = [];
  imgSrc: string = './assets/images/logo-web.png';
  isHandset$: Observable<boolean>;
  isAuthenticated$: Observable<boolean>;
  rule: string = '';

  @ViewChild('drawer') drawer!: MatSidenav;

  constructor(private breakpointObserver: BreakpointObserver,
              public readonly authService: AuthService) {
    this.isAuthenticated$ = authService.isAuthenticated();
    this.isHandset$ = this.breakpointObserver.observe(['(max-width: 1024px)'])
      .pipe(
        map(result => result.matches),
        shareReplay()
      );
  }

  ngOnInit(): void {
    this.isAuthenticated$.subscribe(res =>
      {
        if(res) {
          this.rule = this.authService.readRole();
        }
      });
    this.navigationLink = [
      {route: '/admin', label: 'Acceuil', icon: 'home', children: [], show: true},
      {
        route: '/admin', label: 'Gestion des utilisateurs', icon: '',
        children: [
          {
            route: '/admin/roles/list-roles',
            label: 'Liste des utilisateurs',
            icon: 'list',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' ? true : false
          }
        ],
        show: this.rule ===  'ROLE_SUPER_ADMIN' ? true : false
      },
      {
        route: '/admin', label: 'Gestion des articles', icon: '',
        children: [
          {
            route: '/admin/articles/list-article',
            label: 'Liste des articles',
            icon: 'list',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' ? true : false
          },
          {
            route: '/admin/articles/fiche-article/new',
            label: 'Créer un article',
            icon: 'add',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' ? true : false
          }
        ], show: this.rule ===  'ROLE_SUPER_ADMIN' ? true : false
      },
      {
        route: '/admin', label: 'Gestion des clients', icon: '',
        children: [
          {
            route: '/admin/clients/list-client',
            label: 'Liste des clients',
            icon: 'list',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_CUSTOMER_ORDER' || this.rule === 'ROLE_PURE_CONSULTATION' ? true : false
          }
        ], show: this.rule ===  'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_CUSTOMER_ORDER' || this.rule === 'ROLE_PURE_CONSULTATION' ? true : false
      },
      {
        route: '/admin', label: 'Gestion des modèles', icon: '',
        children: [
          {
            route: '/admin/modeles/list-modele',
            label: 'Liste des modèles',
            icon: 'list',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_STOCK' || this.rule === 'ROLE_PURE_CONSULTATION' ? true : false
          }
        ],
        show: this.rule ===  'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_STOCK' || this.rule === 'ROLE_PURE_CONSULTATION' ? true : false
      },
      {
        route: '/admin',
        label: 'Gestion des matières',
        icon: '',
        children: [
          {
            route: '/admin/matieres/list-matieres',
            label: 'Liste des matières',
            icon: 'list',
            children: [],
            show: this.rule ===  'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_STOCK' || this.rule === 'ROLE_PURE_CONSULTATION'
          },
        ],
        show: this.rule === 'ROLE_SUPER_ADMIN' || this.rule === 'ROLE_MANAGEMENT_STOCK' || this.rule === 'ROLE_PURE_CONSULTATION'
      }
    ];
  }

  toggle() {
    console.log('toggle');
    this.drawer.toggle().then();
  }
}
