import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-delete-confirmation-dailog',
  templateUrl: './delete-confirmation-dailog.component.html',
  styleUrls: ['./delete-confirmation-dailog.component.scss']
})
export class DeleteConfirmationDailogComponent {
  message !: string;
  title !: string;
  constructor(
    private dialogRef: MatDialogRef<DeleteConfirmationDailogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
    if (data) {
      this.message = data.message || this.message;
      this.title = data.title || this.title;

    }
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }

}
