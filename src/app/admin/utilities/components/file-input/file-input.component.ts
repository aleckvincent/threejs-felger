import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {FilesizePipe} from '../../pipes/filesize.pipe';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss'],
  providers: [
    FilesizePipe
  ]
})
export class FileInputComponent implements OnInit {
  @Input() formName!: FormGroup;
  @Input() placeholder!: string;
  @Input() accept!: string;
  @Input() uploadMultiple!: boolean;
  @Input() isModal!: boolean;
  @Input() previewImage!: boolean;
  @Input() isEdit!: boolean;
  @Input() isRequired!: boolean;
  @Input() fileErrorRequired !: boolean | false;
  @Input() sizeMax!: number;
  @Input() isImg!: boolean;
  filenames!: string | null;
  typefile!: string;
  myFiles: File [] = [];
  imageSrc: any [] = [];
  extentions: string [] = [];
  fileTypeError = false;
  fileSizeError = false;
  bytes!: number;
  @ViewChild('UploadFileInput') uploadFileInput!: ElementRef;
  @Output() filesAdd = new EventEmitter<Array<File>>();
  constructor(private fb: FormBuilder, private filesizePipe: FilesizePipe) {
  }

  @Input() set filename(value: string | null | Array<string>) {
    if (value instanceof Array) {
      this.filenames = '';
      value.forEach((item) => {
        this.filenames += item + ',';
      });
    } else {
      this.filenames = value;
    }
    this.initForm();
  }

  @Input() set fileUpload(value: File | null | Array<Observable<File>>) {
    if (value instanceof Array) {
      value.forEach((item) => {
        item.subscribe(res => {
          this.myFiles.push(res as File);
          const reader = new FileReader();
          reader.readAsDataURL(res);
          reader.onload = (e: any) => {
            this.imageSrc.push({name: res, path: e.target.result});
          };
        });
      });
    } else {
      if (value != null) {  this.myFiles.push(value as File) ; }
    }
    this.filesAdd.emit(this.myFiles);
  }

  ngOnInit(): void {
    this.initForm();
    this.filenames = '';
  }
  initForm(): void {
    this.formName = this.fb.group({
      file: [this.filenames ? this.filenames : null, this.isRequired ? Validators.required : '']
    });
  }
  fileChangeEvent(fileInput: any): void {
    if (!this.uploadMultiple) {
      this.myFiles = [];
      this.imageSrc = [];
      this.filenames = '';
    }
    this.fileTypeError = false;
    this.fileSizeError = false;
    const files = [...fileInput.target.files];
    if (this.accept !== '') {
      this.extentions = this.accept.split(',');
    }

    files.forEach((item: any, index: number) => {
      if (item) {
        this.myFiles.push(item);
        this.typefile = item.type.split('/')[1] || item.name.split('.').pop();
        this.bytes = this.filesizePipe.transform(item.size, 'MB');
        this.fileSizeError = this.bytes > this.sizeMax;
        this.fileTypeError = this.extentions.length > 0 && !this.extentions.includes(this.typefile);
        if (!this.fileTypeError && !this.fileSizeError) {
          this.filenames += item.name + ',';
          const reader = new FileReader();
          reader.readAsDataURL(item);
          reader.onload = (e: any) => {
            this.imageSrc.push({name: item, path: e.target.result});
          };
          this.fileErrorRequired = false;
          this.uploadFileInput.nativeElement.value = '';
        }
      }
    });
    this.filesAdd.emit(this.myFiles);
    this.formName = this.fb.group({
      file: [this.filenames ? this.filenames.slice(0, -1) : null, this.isRequired ? Validators.required : '']
    });

  }
  removeFile(): void {
    this.myFiles = [];
    this.filenames = '';
    this.imageSrc = [];
    this.formName = this.fb.group({
      file: [this.filenames ? this.filenames : null, this.isRequired ? Validators.required : '']
    });
    this.filesAdd.emit(this.myFiles);
  }
  removeFiles(file: any): void {
    const index: number = this.myFiles.indexOf(file);
    if (index !== -1) {
      this.myFiles.splice(index, 1);
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (e: any) => {
      this.imageSrc.forEach((img, i) => {
        if (img.name === file && img.path === e.target.result) {
          this.imageSrc.splice(i, 1);
        }
      });
    };
    if (this.filenames !== null) {
      this.filenames = this.filenames.replace(file.name + ',', '');
    }
    this.formName = this.fb.group({
      file: [this.filenames ? this.filenames : null, this.isRequired ? Validators.required : '']
    });
    this.filesAdd.emit(this.myFiles);
  }
}
