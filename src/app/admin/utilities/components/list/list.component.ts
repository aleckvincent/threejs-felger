import {Component, EventEmitter, Input, OnInit, Output, AfterViewInit, ViewChild} from '@angular/core';
import {Action} from '../../models/action';
import {IkeyValue} from '../../models/ikey-value';
import {Observable} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { AuthService } from 'src/app/admin/services/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, AfterViewInit {
  @Input() dataSource$!: Observable<Array<any>>;
  @Input() actionList!: Array<Action>;
  @Input() rowColumns!: Array<IkeyValue<string>>;
  @Input() headerColumns!: Array<string>;
  @Output() callEditFunction: EventEmitter <any> = new EventEmitter<any>();
  @Output() callDeleteFunction: EventEmitter <any> = new EventEmitter<any>();
  @Output() callActivateFunction: EventEmitter <any> = new EventEmitter<any>();
  @Output() callDeactivateFunction: EventEmitter <any> = new EventEmitter<any>();
  @Output() callShowFunction: EventEmitter <any> = new EventEmitter<any>();
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  datalist = new MatTableDataSource<any>();
  visibility: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.rowColumns.push({uuid: 'actions', label: 'Actions'});
    this.headerColumns.push('actions');
    const rule = this.authService.readRole();
    this.visibility = rule === 'ROLE_SUPER_ADMIN' || rule === 'ROLE_MANAGEMENT_STOCK' || rule === 'ROLE_MANAGEMENT_CUSTOMER_ORDER' ? true : false;
  }
  ngAfterViewInit(): void {
    this.dataSource$.subscribe(res => {
      this.datalist.data = res;
      this.datalist.paginator = this.paginator;
      this.datalist.sort = this.sort;
    });
  }
  chooseMethod(action: any, uuid: string): void {
    switch ( action.method ) {
      case 'show':
        this.callShowFunction.emit(uuid);
        break;
      case 'edit':
        this.callEditFunction.emit(uuid);
        break;
      case 'delete':
        this.callDeleteFunction.emit(uuid);
        break;
      case 'activate':
        this.callActivateFunction.emit(uuid);
        break;
      case 'deactivate':
        this.callDeactivateFunction.emit(uuid);
        break;
      default:
        break;
    }
  }
}
