import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input('isHandset') public isHandset !: boolean|null;
  @Output('toggle') public toggle: EventEmitter<any> = new EventEmitter();
  public jwtUser!: string | null;
  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.jwtUser = sessionStorage.getItem('username');
  }
  toggleDrawer() {
    this.toggle.emit();
  }
  deconnect(): void {
    sessionStorage.setItem('token', '');
    this.router.navigate(['/admin/auth/login']);
  }
}
