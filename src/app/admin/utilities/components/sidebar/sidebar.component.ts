import {Component, Input, OnInit} from '@angular/core';
import {Link} from '../../models/link';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

interface LinkNode {
  expandable: boolean;
  name: string;
  level: number;
  route: string;
  icon: string;
  show: boolean;
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input('isHandset') public isHandset!: boolean | null;
  @Input('logo') public logo!: string;
  @Input('liens') public liens!: Array<Link>;

  private _transformer = (node: Link, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.label,
      level: level,
      route: node.route,
      icon: node.icon,
      show: node.show
    };
  }

  treeControl = new FlatTreeControl<LinkNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
  }
  hasChild = (_: number, node: LinkNode) => node.expandable;
  ngOnInit(): void {
    this.dataSource.data = this.liens;
  }
}
