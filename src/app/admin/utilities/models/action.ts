export interface Action {
  name: string;
  method: string;
  icon: string;
}
