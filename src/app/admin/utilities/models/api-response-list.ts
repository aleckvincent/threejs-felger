export interface ApiResponseList<T> {
  status: number;
  data: Array<T>;
}
