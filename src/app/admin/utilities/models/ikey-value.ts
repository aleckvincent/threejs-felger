export interface IkeyValue<T> {
  uuid: T;
  label: string;
}
