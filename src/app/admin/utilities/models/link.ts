export interface Link {
  route: string;
  label: string;
  icon: string;
  children: Link [];
  show: boolean;
}
