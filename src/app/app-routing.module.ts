import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ViewerComponent} from './vitrine/viewer/viewer.component';

const routes: Routes = [
  {path: '', loadChildren: () => import('./vitrine/vitrine.module').then(mod => mod.VitrineModule)},
  {path: 'admin', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule)},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
