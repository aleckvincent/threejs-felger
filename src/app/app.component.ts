import { Component } from '@angular/core';
import {SnackBarService} from './admin/services/snack-bar.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Maison-Felger';
  constructor(private notificationService: SnackBarService, private snackBar: MatSnackBar) {
    this.notificationService.notification$.subscribe(message => {
      this.snackBar.open(message,
        'X',
        {
          duration: 3000,
          panelClass: ['success-snackbar']
        });
    });
    this.notificationService.notificationError$.subscribe(message => {
      this.snackBar.open(message,
        'X',
        {
          duration: 3000,
          panelClass: ['error-snackbar']
        });
    });
  }
}
