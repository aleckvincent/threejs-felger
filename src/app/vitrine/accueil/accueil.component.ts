import {Component, ElementRef,HostListener, Inject, OnInit} from '@angular/core';
import {PageScrollService} from "ngx-page-scroll-core";
import {DOCUMENT} from "@angular/common";
import {NewsletterComponent} from "../newsletter/newsletter.component";
import {MatDialog} from "@angular/material/dialog";
import {NewsletterService} from "../shared/services/newsletter.service";
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  bannerClass:string = "fullHeight";
  activeSection = 1;
  curScroll: number =0;
  displayHalfHeight: string ='hide';
  public hpImages: Array<string> = [
    'assets/images/hp-1.jpg',
    'assets/images/hp-2.jpg',
    'assets/images/hp-3.jpg',
    'assets/images/hp-4.jpg'
  ];
  public excellenceImages: Array<string> = [
    'assets/images/excellence-1.jpg',
    'assets/images/excellence-2.png'
  ];

  public excellenceStyle = "zoom";
  public excellenceDetail: Array<string> = [
    'assets/images/3-700.gif'
  ]

  constructor(
    private pageScrollService: PageScrollService,
    private dialog: MatDialog,
    private newsletterService: NewsletterService,
    private _snackBar: MatSnackBar,
    @Inject(DOCUMENT) private document: any
  ) {
  }

  ngOnInit(): void {
    console.log(this.displayHalfHeight)
    setTimeout(()=>{
      this.newsletter();
    }, 6000);
  }


  fullPageScroll(e: HTMLElement, i: number) {
    this.pageScrollService.scroll({
      scrollTarget: e,
      document: this.document
    });

    this.activeSection = i;
  }

  @HostListener('window:scroll')
  checkScroll() {
    this.curScroll = window.scrollY || document.documentElement.scrollTop;
    this.toggleBannerDisplay(this.curScroll)
  }

  toggleBannerDisplay( curScroll: number) {
    if (curScroll > 60) {
      this.bannerClass = 'halfHeight';
      this.displayHalfHeight = 'display';
    }
    console.log(this.displayHalfHeight)
  }


  newsletter() {
    this.dialog.open(NewsletterComponent, {
      closeOnNavigation: true,
      width: '700px',
      height: '450px',
      backdropClass: 'backdropBackground',
      disableClose: false
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.newsletterService.subscribeNewsLetter(result.email, result.firstname, result.lastname)
        .subscribe((res) => {
            this._snackBar.open('Votre inscription a bien été prise en compte.', '', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            });
          },
          (err) =>  {
            this._snackBar.open(err.message, '', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            });
            console.log(err);
          })
      }
    })
  }

}
