import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ApercuArticleComponent} from './containers/apercu-article/apercu-article.component';
import {ArticleResolver} from './resolver/article.resolver';

const routes: Routes = [
  {path: ':slogan', component: ApercuArticleComponent, resolve: { article: ArticleResolver }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
