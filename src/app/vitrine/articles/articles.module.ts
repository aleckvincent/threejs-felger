import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ArticlesRoutingModule} from './articles-routing.module';
import {ApercuArticleComponent} from './containers/apercu-article/apercu-article.component';
import {MatCardModule} from '@angular/material/card';
import {VitrineModule} from '../vitrine.module';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

@NgModule({
  declarations: [
    ApercuArticleComponent,
    SafeHtmlPipe
  ],
  imports: [
    CommonModule,
    ArticlesRoutingModule,
    MatCardModule,
    VitrineModule
  ]
})
export class ArticlesModule {
}
