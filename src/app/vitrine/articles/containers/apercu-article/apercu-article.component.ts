import {Component, OnInit} from '@angular/core';
import {ArticleDto} from '../../../../admin/articles/models/article-dto';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ArticleService} from '../../../../admin/articles/services/article.service';
import {DocumentService} from '../../../../admin/clients/services/document.service';
import {SafeHtmlPipe} from '../../pipes/safe-html.pipe';

@Component({
  selector: 'app-apercu-article',
  templateUrl: './apercu-article.component.html',
  styleUrls: ['./apercu-article.component.scss'],
  providers: [
    SafeHtmlPipe
  ]
})
export class ApercuArticleComponent implements OnInit {
  public slogan!: string;
  public article!: ArticleDto;
  bannerClass = 'fullHeight';
  path = '';
  constructor(private route: ActivatedRoute, private toastr: ToastrService,
              private readonly articleService: ArticleService, private readonly documentService: DocumentService) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
        this.article = data.article;
      },
      error => {
        if (error.status === 400 && error.error.errors) {
          const errors = error.error.errors;
          for (let i = 0; i < errors.length; i++) {
            if (errors[i].key === '') {
              this.toastr.error(errors[i].code);
              return;
            }
          }
        }
    });
    if (this.article.image.toString() !== '') {
      this.documentService.getDocumentInfo(this.article.image.toString()).subscribe(
        (res: any) => {
          this.documentService.getDocument(this.article.image.toString(), res.originalName, res.mimeType).subscribe(response => {
            const reader = new FileReader();
            reader.readAsDataURL(response);
            reader.onload = (e: any) => {
              this.path = e.target.result;
            };
          });
        });
    } else {
      this.path = '../../assets/svg/logoMFbg.svg';
    }
  }
}
