import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {ArticleService} from '../../../admin/articles/services/article.service';
import {Observable, throwError} from 'rxjs';
import {ArticleDto} from '../../../admin/articles/models/article-dto';

@Injectable({
  providedIn: 'root'
})
export class ArticleResolver implements Resolve<ArticleDto[]> {
  constructor(private articleService: ArticleService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ArticleDto> | ArticleDto[] | any {
    const id = route.paramMap.get('slogan');
    if (!!id) {
      return this.articleService.getArticleBySlogan(id);
    }
    throwError(new Error('article not found'));
  }
}
