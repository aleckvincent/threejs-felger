import {Component, OnInit} from '@angular/core';
import {ReservationComponent} from "../reservation/reservation.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  public images: Array<string> = [
    'assets/images/10_2_12.jpg',
    'assets/images/10_1_21.jpg'
  ];

  public carouselStyle = "zoom";

  constructor(private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  prendreRdv() {
    this.dialog.open(ReservationComponent, {
      closeOnNavigation: true,
      width: '860px',
      height: 'auto',
      disableClose: false
    })
  }
}
