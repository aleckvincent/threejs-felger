import { Component, OnInit } from '@angular/core';
import {Infos} from "../shared/constants/infos";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-mentions-legales',
  templateUrl: './mentions-legales.component.html',
  styleUrls: ['./mentions-legales.component.scss']
})
export class MentionsLegalesComponent implements OnInit {
  public infos: Infos = new Infos();
  constructor(public matDialogRef: MatDialogRef<MentionsLegalesComponent>) { }

  ngOnInit(): void {
  }

  closeModale() {
    this.matDialogRef.close();
  }
}
