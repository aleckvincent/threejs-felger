import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {NewsletterService} from '../shared/services/newsletter.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  form !: FormGroup;
  constructor(
    private build: FormBuilder,
    public matDialogRef: MatDialogRef<NewsletterComponent>
  ) { }

  ngOnInit(): void {
    const emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    this.form = this.build.group({
      fistname: ['', [Validators.required, Validators.maxLength(60)]],
      lastname: ['', [Validators.maxLength(60), Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(emailregex)]],
    });
  }

  closeModale(): void {
    this.matDialogRef.close();
  }

  sendData(): void {
    if (this.form.valid) {
      this.matDialogRef.close({
        firstname: this.form.get('fistname')?.value,
        lastname: this.form.get('lastname')?.value,
        email: this.form.get('email')?.value
      });
    }
  }
}
