import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../shared/services/article.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {DocumentService} from '../../admin/clients/services/document.service';

@Component({
  selector: 'app-nos-article',
  templateUrl: './nos-article.component.html',
  styleUrls: ['./nos-article.component.scss']
})
export class NosArticleComponent implements OnInit {
  public articles$!: Observable<Array<any>>;
  imagesInfos: any = [];
  constructor(private readonly articleService: ArticleService,
              private readonly documentService: DocumentService, private router: Router) { }

  ngOnInit(): void {
    this.articles$ = this.articleService.getArticles();
    this.articles$.subscribe(data => {
      for (const img of data) {
        if (img.image.toString() !== '') {
          this.documentService.getDocumentInfo(img.image.toString()).subscribe(
            (res: any) => {
              this.documentService.getDocument(img.image, res.originalName, res.mimeType).subscribe(response => {
                const reader = new FileReader();
                reader.readAsDataURL(response);
                reader.onload = (e: any) => {
                  this.imagesInfos.push({name: img.image, path: e.target.result});
                };
              });
            });
        }
      }
    });
  }

  showArticle(slogan: string): void {
    const url = this.router.serializeUrl(this.router.createUrlTree(['/article', slogan]));
    window.open(url);
  }
}
