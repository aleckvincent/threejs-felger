import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotreOffreComponent } from './notre-offre.component';

describe('NotreOffreComponent', () => {
  let component: NotreOffreComponent;
  let fixture: ComponentFixture<NotreOffreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotreOffreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotreOffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
