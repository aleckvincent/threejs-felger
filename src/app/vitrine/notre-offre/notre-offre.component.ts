import {Component, OnInit} from '@angular/core';
import {ReservationComponent} from '../reservation/reservation.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-notre-offre',
  templateUrl: './notre-offre.component.html',
  styleUrls: ['./notre-offre.component.scss']
})
export class NotreOffreComponent implements OnInit {

  public currentOffre = 0;
  public selectedItemIndex = 0;
  public panelOpenState = false;
  public offres = [
    {

      img: 'assets/images/offreComptemporain.jpg',
      name: 'Contemporain',
      type: 'MOCASSIN, SNEAKERS',
      price: 'à partir de 880 €',
      title: 'Un look décontracté avec les souliers Contemporain',
      content: {
        p1: 'Alliant confort et allure, nos mocassins et nos sneakers vous permettront d’accessoiriser vos tenues\n' +
          ' chics et casuals, tout en préservant un style élégant.',
        p2: 'Une paire de mocassins pour un style décontracté ou des sneakers blanches pour un look plus sportswear,\n' +
          ' à vous de définir votre allure en personnalisant votre paire.',
        p3: ''
      }
    },
    {
      img: 'assets/images/offreIntemporel.jpg',
      name: 'Intemporel',
      type: 'RICHELIEUS, DERBIES, DOUBLE BOUCLES',
      price: 'à partir de 1280 €',
      title: 'L\'offre intemporelle pour un look plus élaboré',
      content: {
        p1: 'Notre Monk à double boucles, nos Derbies et nos Richelieus sauront vous assurer confort et élégance au\n' +
          'travail ou lors de vos événements. Idéal pour la mi-saison, ces souliers sont devenus un grand classique\n' +
          'du vestiaire masculin.',
        p2: 'Ces trois souliers peuvent aussi bien convenir à un look décontracté que tendance, portés avec un chino\n' +
          'ou un jean.',
        p3: ''
      }
    },
    {
      img: 'assets/images/offreHeritage.jpg',
      name: 'Héritage',
      type: 'CHUKKA BOOTS, CHELSEA BOOTS',
      price: 'à partir de 1380 €',
      title: 'L\'Héritage, pour arborer un style moderne au quotidien',
      content: {
        p1: 'Issus d’univers bien différents nos boots offrent confort et style et s’adaptent à chaque look qu’il\n' +
          ' soit urbain, branché ou élégant...',
        p2: 'Nos boots s’associent aussi bien avec un pantalon slim, une chemise et une veste de costume pour un\n' +
          ' look sophistiqué qu’avec un jean et un tee-shirt pour un look décontracté.',
        p3: ''
      }
    }
  ];

  constructor(private dialog: MatDialog) {
  }

  closeContent(): void {
    this.panelOpenState = false;
    const element: Element | null = document.documentElement.querySelector(`#offreMobile`);
    element?.scrollIntoView({behavior: 'smooth', inline: 'nearest'});

  }

  showContent(index: number): void {
    this.panelOpenState = false;
    setTimeout(() => {
      this.selectedItemIndex = index;
      this.panelOpenState = true;
    }, 1000);
    const element: Element | null = document.documentElement.querySelector(`#offreDiscription`);
    element?.scrollIntoView({behavior: 'smooth', inline: 'nearest'});
  }

  public changeTo(distination: string): void {
    if (distination === 'previous') {
      this.currentOffre = this.currentOffre !== 0 ? this.currentOffre - 1 : 3;
    } else if (distination === 'next') {
      this.currentOffre = this.currentOffre !== 3 ? this.currentOffre + 1 : 0;
    }
  }

  prendreRdv(): void {
    this.dialog.open(ReservationComponent, {
      closeOnNavigation: true,
      width: '860px',
      height: 'auto',
      disableClose: false
    });
  }

  ngOnInit(): void {
  }

}
