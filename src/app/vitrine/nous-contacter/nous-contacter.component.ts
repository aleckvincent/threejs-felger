import {Component, OnInit} from '@angular/core';
import {Infos} from "../shared/constants/infos";
import {FormControl, Validators} from "@angular/forms";
import {NewsletterService} from "../shared/services/newsletter.service";
import {MatDialog} from "@angular/material/dialog";
import {MentionsLegalesComponent} from "../mentions-legales/mentions-legales.component";
import {ReservationComponent} from "../reservation/reservation.component";
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-nous-contacter',
  templateUrl: './nous-contacter.component.html',
  styleUrls: ['./nous-contacter.component.scss']
})
export class NousContacterComponent implements OnInit {

  public infos: Infos = new Infos();
  email = new FormControl('', [Validators.required, Validators.email]);
  inputClass = "";
  durationInSeconds = 5;

  constructor(
    private newsletterService: NewsletterService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar) {
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'Vous devez saisir un email';
    }

    return this.email.hasError('email') ? 'email n\'est pas valide' : '';
  }

  ngOnInit(): void {
  }

  setActive(){
    this.email.value != "" ? this.inputClass = 'active' : this.inputClass = '';
  }

  addNewsLetter() {
    if (this.email.valid) {
      this.newsletterService.subscribeNewsLetter(this.email.value, '', '')
        .subscribe((res) => {
            this.email.reset();
            this.email.markAllAsTouched();
            this.email.markAsPristine();
            this._snackBar.open('Votre inscription a bien été prise en compte.', '', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            });
          },
          (err) =>  {
            this._snackBar.open(err.message, '', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            });
            console.log(err);
          })
    }
  }

  openMentionsLegales() {
    this.dialog.open(MentionsLegalesComponent, {
      closeOnNavigation: true,
      width: '880px',
      maxHeight: '90vh',
      disableClose: false
    })
  }

  prendreRdv() {
    this.dialog.open(ReservationComponent, {
      closeOnNavigation: true,
      width: '860px',
      height: '90vh',
      disableClose: false
    })
  }
}