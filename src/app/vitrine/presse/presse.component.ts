import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-presse',
  templateUrl: './presse.component.html',
  styleUrls: ['./presse.component.scss']
})
export class PresseComponent implements OnInit {

  public currentArticle: number = 0;
  public articles = [
    {"article": "“Férue d'innovation et de luxe à la française, Maria Karunagaran, patronne de R&K,\n" +
        "      créé via la technologie 3D des souliers sur mesure, et se lance dans la production\n" +
        "      « made in France » avec l'ouverture de son usine en Bretagne...”",
    "writer": "JULIE LE BOLZER",
    "presse": "LES ECHOS"},
    {"article": "“...Nec plus ultra: la personnalisation extrême chez R&K (...) la marque propose un service\n" +
        "      sur mesure autour de 10 intemporels du vestiaire masculin.”",
      "writer": "",
      "presse": "MARIANNE"},
    {"article": "“...une marque de souliers de luxe, exclusivement pour hommes, va installer son atelier de productions\n" +
        "      près de Fougères, à Parigné, redorant le blason de la cité de la chaussure.”",
      "writer": "ANTOINE VICTOT",
      "presse": "OUEST FRANCE"},
    {"article": "“Créer une chaussure sur-mesure à partir d’un scanner en 3D du pied, c’est ce que propose la marque R&K à la gent masculine…”",
      "writer": "",
      "presse": "FASHION NETWORK"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  public changeTo(distination: string){
    if(distination === "previous") {
      this.currentArticle = this.currentArticle !== 0 ? this.currentArticle - 1 : 3 ;
    } else if (distination === "next"){
      this.currentArticle = this.currentArticle !== 3 ? this.currentArticle + 1 : 0 ;
    }
}

}
