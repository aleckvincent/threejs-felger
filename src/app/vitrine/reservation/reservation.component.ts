import {Component, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {AppointmentService} from "../shared/services/appointment.service";
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {ErrorStateMatcher} from "@angular/material/core";
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material/snack-bar';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  selectedDate: Date;
  selectedHour = '';
  form !: FormGroup;
  availableTimes: any[] = [
    {hour: '09:00', isDisabled: true},
    {hour: '10:00', isDisabled: true},
    {hour: '11:00', isDisabled: true},
    {hour: '12:00', isDisabled: true},
    {hour: '13:00', isDisabled: true},
    {hour: '14:00', isDisabled: true},
    {hour: '15:00', isDisabled: true},
    {hour: '16:00', isDisabled: true},
    {hour: '17:00', isDisabled: true},
    {hour: '18:00', isDisabled: true},
    {hour: '19:00', isDisabled: true}
  ];
  step: string = 'appointment';
  messageErrors = '';
  minDate = moment('15/05/2021', 'DD/MM/YYYY').toDate();
  private bookedAppointments: [] = [];

  constructor(private appointmentService: AppointmentService,
              private datepipe: DatePipe,
              private dialogRef: MatDialogRef<ReservationComponent>,
              private build: FormBuilder,
              private _snackBar: MatSnackBar) {
    this.selectedDate = this.minDate;
  }

  ngOnInit(): void {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.form = this.build.group({
      'client': this.build.group({
        'name': ['', [Validators.required, Validators.maxLength(60)]],
        'lastName': ['', [Validators.maxLength(60), Validators.required]],
        'email': ['', [Validators.required, Validators.email, Validators.pattern(emailregex)]],
        'phone': ['', Validators.required],
        'address': [''],
        'ville': [''],
        'zipCode': [''],
      }),
      'appointment': this.build.group({
        'date': [null, Validators.required],
        'hour': [null, Validators.required]
      })
    })
    this.loadAppointments(this.minDate);
  }


  loadAppointments(date: Date): void {
    this.selectedDate = date;
    const casted_date = String(this.datepipe.transform(this.selectedDate, 'dd/MM/yyyy'));
    (this.form.get('appointment') as FormGroup).get('date')?.setValue(casted_date);

    this.appointmentService.getBookedAppointment()
      .subscribe(
        (res) => {
          this.bookedAppointments = res.data.bookedAppointments;
          this.availableTimes.map(function (availableTime) {
            availableTime.isDisabled = res.data.bookedAppointments[casted_date] ?
              res.data.bookedAppointments[casted_date].includes(availableTime.hour) : false
            return availableTime;
          })
        },
        (err) => err.error?.map((item: any) => this.messageErrors += item)
      );
  }

  valide() {
    if (this.form.valid) {
      this.appointmentService.saveAppointment(this.form.getRawValue())
        .subscribe((res) => {
          this.step = 'success' ;
          setTimeout(() => {
            this._snackBar.open('Rendez-vous est bien enregistré.', '', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            });

          }, 3000);

        },
          (err) => err.error?.map((item: any) => this.messageErrors += item)
        );
    } else {
      for (const err in this.form.errors)
        if (this.form.errors) {
          this.messageErrors += this.form.errors[err] + "\n";
        }
    }
  }

  back() {
    this.step = 'appointment';
    this.loadAppointments(this.selectedDate);
  }

  next() {
    if ((this.form.get('appointment') as FormGroup).get('hour')?.valid &&
      (this.form.get('appointment') as FormGroup).get('date')?.valid) {
      this.messageErrors = '';
      this.step = 'customer';
    } else {
      this.messageErrors = 'Veuillez choisir un rendez-vous!'
    }
  }

  close() {
    this.dialogRef.close();
  }
}
