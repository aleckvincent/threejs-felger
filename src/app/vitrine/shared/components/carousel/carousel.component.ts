import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input('images') images: Array<string> = [];
  @Input('delay') delay: number = 4000;
  @Input('style') style:string = 'default'
  currentImage!: string;
  currentIndex: number = 0

  constructor() {
  }

  ngOnInit(): void {
    this.currentImage = this.images[this.currentIndex];
    if (this.images.length > 1) {
      setInterval(() => {
        if (this.currentIndex < this.images.length - 1) {
          this.currentIndex += 1;
        } else {
          this.currentIndex = 0;
        }
        this.currentImage = this.images[this.currentIndex];
      }, this.delay)
    }
  }

}
