import { Component, OnInit, HostListener } from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {MatDialog} from "@angular/material/dialog";
import {MentionsLegalesComponent} from "../../../mentions-legales/mentions-legales.component";

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit {
  public showCookiesBar: boolean = false;
  constructor(
    private cookieService: CookieService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (!this.cookieService.get('acceptCookies')) {
      this.showCookiesBar = true;
      this.cookieService.set('acceptCookies', "1");
    }
  }

  @HostListener('window:scroll', ['$event'])
  public hideCookiesBar() {
    this.showCookiesBar = false;
  }

  openMentionsLegales() {
    this.dialog.open(MentionsLegalesComponent, {
      closeOnNavigation: true,
      width: '860px',
      maxHeight: '90vh',
      disableClose: false
    })
  }
}
