import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hamburger',
  template: `
    <div id="menu-toggle"  class="{{state}}">
      <div id="hamburger">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div id="cross">
        <span></span>
        <span></span>
      </div>
    </div>
  `,
  styleUrls: ['./hamburger.component.scss']
})
export class HamburgerComponent implements OnInit {

  constructor() { }

  @Input('state') state: string = 'headerClose';

  ngOnInit(): void {
  }

  togglePanel(): void{

    this.state === "headerClose" ? this.state = "headerOpen" : this.state = "headerClose";
  }
}

