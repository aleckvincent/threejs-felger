import {AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {ReservationComponent} from '../../../reservation/reservation.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  panelOpenState = false;
  panelIcon: string = 'assets/svg/menu.svg';
  panelMobileLogo: string = 'none';
  menuClosedClass: string = 'headerClose';
  prevScroll!: number;
  curScroll!: number;
  direction = 0;
  prevDirection = 0;
  displayLogo = 'none';
  displayLogoMobile = 'none';
  @ViewChild('header', {static: true}) header!: ElementRef;
  @ViewChild('menu', {static: true}) menu!: ElementRef;


  constructor(@Inject(DOCUMENT) private document: any,
              private dialog: MatDialog) {

    this.prevScroll = window.scrollY || document.documentElement.scrollTop;
  }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
  }

  prendreRdv(): void {
    this.dialog.open(ReservationComponent, {
      closeOnNavigation: true,
      width: '860px',
      height: '90vh',
      disableClose: false
    });
  }

  @HostListener('window:scroll')
  checkScroll(): void {
    /*
    ** Find the direction of scroll
    ** 0 - initial, 1 - up, 2 - down
    */

    this.curScroll = window.scrollY || document.documentElement.scrollTop;
    if (this.curScroll > this.prevScroll) {
      //scrolled up
      this.direction = 2;
    } else if (this.curScroll < this.prevScroll) {
      //scrolled down
      this.direction = 1;
    }

    if (this.direction !== this.prevDirection || this.curScroll <= 60) {
      this.toggleHeader(this.direction, this.curScroll);
      /* this.panelOpenState ? this.displayLogoMobile = 'none' : 'initial'; */
    }

    this.prevScroll = this.curScroll;
    this.closePanel();


  }

  togglePanel(): void {
    this.panelIcon = this.panelOpenState ? 'assets/svg/menu.svg' : 'assets/svg/close.svg';
    this.panelMobileLogo = this.panelOpenState ? 'none' : 'initial';
    this.menuClosedClass = this.panelOpenState ? 'headerClose' : 'headerOpen';
    this.panelOpenState = !this.panelOpenState;
  }

  openPanel(): void {
    this.panelOpenState = false;
    this.panelMobileLogo = 'initial';
    this.menuClosedClass = 'headerOpen';
  }

  closePanel(): void {
    this.panelIcon = 'assets/svg/menu.svg';
    this.panelMobileLogo = 'none';
    this.menuClosedClass = 'headerClose';
    this.panelOpenState = false;
  }

  movePanel(): void {
    if (this.panelOpenState) {
      this.panelIcon = 'assets/svg/menu.svg';
      this.panelMobileLogo = 'none';
      this.menuClosedClass = 'headerOpen';
    } else {
      this.panelIcon = 'assets/svg/close.svg';
      this.panelMobileLogo = 'initial';
      this.menuClosedClass = 'headerMoved';
    }
  }
  leavePanel(): void {
    if (this.panelOpenState) {
      this.panelIcon = 'assets/svg/menu.svg';
      this.panelMobileLogo = 'none';
      this.menuClosedClass = 'headerOpen';
    } else {
      this.panelIcon = 'assets/svg/close.svg';
      this.panelMobileLogo = 'initial';
      this.menuClosedClass = 'headerLeave';
    }
  }
  toggleHeader(direction: number, curScroll: number): void {
    if (direction === 1 && curScroll > 60) {
      this.header.nativeElement.classList.add('sticky');
      this.displayLogo = 'initial';
      this.displayLogoMobile = 'initial';
    } else if (direction === 2 || curScroll <= 60) {
      this.header.nativeElement.classList.remove('sticky');
      /*this.displayLogo = 'none'; */
    }
    this.prevDirection = direction;
  }

  /*   showMenu() {
      this.menu.nativeElement.classList.add('fade-menu');
    }

    hideMenu() {
      this.menu.nativeElement.classList.remove('fade-menu');
    } */

  /*   toggleMenu(){
      let fade =this.menu.nativeElement.classList[1];
      fade === 'fade-menu' ? this.menu.nativeElement.classList.remove('fade-menu') :this.menu.nativeElement.classList.add('fade-menu');
    } */
}
