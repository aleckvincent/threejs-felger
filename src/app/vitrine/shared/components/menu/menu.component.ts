import {AfterContentInit, AfterViewChecked, AfterViewInit, Component, Inject, Input, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {ReservationComponent} from '../../../reservation/reservation.component';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterViewInit {
  @Input('isOpen') isOpen = false;
  isHomePage = true;
  private section$: Observable<any> | undefined;
  constructor(@Inject(DOCUMENT) private document: any, private dialog: MatDialog,
              private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    if (this.route.routeConfig?.path === ':slogan') {
      this.isHomePage = false;
    }
  }
  ngAfterViewInit(): void{
    this.section$ =  this.router.events.pipe(
      map(() => this.router.getCurrentNavigation()?.extras.state)
    );
    this.section$.subscribe(res => {
      if (res) {
        setTimeout(() => this.scrollToElement(res.section), 200);
      }
    });
  }
  prendreRdv(): void {
    this.dialog.open(ReservationComponent, {
      closeOnNavigation: true,
      width: '860px',
      height: '90vh',
      disableClose: false
    });
  }

  scrollToElement(elementID: string): void {
    const element: Element | null = document.documentElement.querySelector(`#${elementID}`);
    element?.scrollIntoView({behavior: 'smooth', inline: 'nearest'});
  }

  redirectToHomePage(elementID: string): void {
    this.router.navigateByUrl('', {state: {section: elementID}});
  }
}
