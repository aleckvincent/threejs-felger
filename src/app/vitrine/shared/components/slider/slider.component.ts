import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SwiperComponent } from 'ngx-useful-swiper';
import { SwiperOptions } from 'swiper';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @Input('images') images: Array<any> = [];
  @Input('type') bgtype = 'background';
  @Output() showContentbyIndex = new EventEmitter<number>();

  config: SwiperOptions = {};
  public showContent = false;

  constructor() { }

  ngOnInit(): void {
    switch (this.bgtype) {
      case 'background':
        this.config = {
          autoHeight: true,
          allowTouchMove: false,
          spaceBetween : 30,
          centeredSlides : true ,
          autoplay: {
            delay: 5000,
            disableOnInteraction: false
          },
          effect: 'fade',
          speed: 1000,
          loop: true
        };
        break;
        case 'carousel':
          this.config = {
            autoHeight: false,
            allowTouchMove: false,
            spaceBetween : 30,
            centeredSlides : true ,
            autoplay: {
              delay: 5000,
              disableOnInteraction: false
            },
            breakpoints: {
              1024: {
                slidesPerView: 1
              },
              500: {
                slidesPerView: 1
              },
              400: {
                slidesPerView: 1
              },
              300: {
                slidesPerView: 1
              }
            },
            effect: 'fade',
            speed: 1000,
            loop: true
          };
          break;

      default:
        this.config = {
          pagination: { el: '.swiper-pagination', clickable: true },
          autoHeight: true,
          allowTouchMove: true,
          autoplay: {
            delay: 5000,
            disableOnInteraction: false
          },
          breakpoints: {
            1024: {
              slidesPerView: 1
            },
            500: {
              slidesPerView: 1
            },
            400: {
              slidesPerView: 1
            },
            300: {
              slidesPerView: 1
            }
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          loop: true
        };
        break;
    }
  }

  ShowContent(slide: any): void {
    let index = -1;
    index = this.images.indexOf(slide);
    console.log(index);
    this.showContentbyIndex.emit(index);
  }
}
