export class Infos {
  public get phoneNumbre(): string {
    return "+33 (0)1 84 60 11 05";
  }

  public get email(): string {
    return "contact@maisonfelger.fr";
  }

  public get address(): string {
    return "99 RUE DU BAC 75007 PARIS";
  }

  public get facebook(): string {
    return "https://www.facebook.com/maisonfelger/";
  }

  public get instagram(): string {
    return "https://www.instagram.com/maisonfelger/";
  }

  public get linkedin(): string {
    return "https://www.linkedin.com/company/maisonfelger/";
  }

  public get webSite(): string {
    return "www.maisonfelger.fr";
  }
}
