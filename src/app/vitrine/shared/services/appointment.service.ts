import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient) {
  }

  get baseUrl(): string {
    return `${environment.apiUrl}/appointment`
  }

  getBookedAppointment(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/booked`);
  }

  saveAppointment(body: any) {
    body.appointment.hour = body.appointment.hour[0];
    return this.http.post(`${this.baseUrl}/new`, body);
  }
}
