import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class NewsletterService {


  constructor(private http: HttpClient) {
  }

  subscribeNewsLetter(email: string, firstname?: string, lastname?: string): Observable<any>{
    const form = {
      email,
      firstname,
      lastname
    };
    return this.http.post(environment.apiUrl + '/news-letter', form);
  }

}
