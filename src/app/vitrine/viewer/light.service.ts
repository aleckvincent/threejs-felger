import { Injectable } from '@angular/core';
import * as THREE from 'three';

@Injectable({
  providedIn: 'root'
})
export class LightService {

  constructor() { }

  public hemisphere(): THREE.HemisphereLight
  {
    return new THREE.HemisphereLight('#ffeeb1', '#080820', 4);
  }

  public spot(color: string, intensity: number): THREE.SpotLight
  {
    const light = new THREE.SpotLight(color, intensity);
    light.castShadow = true;
    return light;
  }

  public directional(scene: THREE.Scene, helper: boolean = false): THREE.DirectionalLight
  {
    const light = new THREE.DirectionalLight('#ffffff', 8);
    if (helper) {
      const helperLight = new THREE.DirectionalLightHelper(light, 5, 0x00000);
      scene.add(helperLight);
    }
    scene.add(light);
    return light;
  }

  public ambient(scene: THREE.Scene): THREE.AmbientLight
  {
    const light = new THREE.AmbientLight('#ffffff', 6);
    scene.add(light);
    return light;
  }
}
