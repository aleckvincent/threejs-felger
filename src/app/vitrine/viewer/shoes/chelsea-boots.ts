import {Shoes} from './shoes';

export class ChelseaBoots implements Shoes{
  public name = 'Chelsea Boots';
  public path = 'test_boots_midlle_poly_maison_felger';


  public getConfigurableParts(): any {
    return [
      {
        name: 'Cuir',
        meshName: 'enveloppe_extérieure_Mesh.001',
        colors: [{
          displayName: 'stealBlue',
          hexCode: '#3581B8'
        },
        {
          displayName: 'macaroniCheese',
          hexCode: '#FCB07E'
        },
        {
          displayName: 'oxfordBlue',
          hexCode: '#011638'
        }
        ]}
    ];
  }
}
