import {Shoes} from './shoes';

export class Richelieu implements Shoes{
  public name = 'Richelieu';
  public path = 'richelieu_bogue_2';

  public getConfigurableParts(): any {
    return [
      {
        name: 'Semelle exterieure',
        meshName: 'semelle_Mesh',
        colors: [{
          displayName: 'darkPurple',
          hexCode: '#1E0F1C'
        },
          {
            displayName: 'darkBlue',
            hexCode: '#384454'
          },
          {
            displayName: 'darkGreen',
            hexCode: '#26474E'
          }
        ]
      }
    ];
  }
}
