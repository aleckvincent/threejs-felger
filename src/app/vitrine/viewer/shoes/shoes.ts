export interface Shoes {
  name: string;
  path: string;
  getConfigurableParts(): any;
}
