import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import * as THREE from 'three';
import {WebGLRenderer} from 'three';
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {MTLLoader} from 'three/examples/jsm/loaders/MTLLoader';
import {Richelieu} from './shoes/richelieu';
import {ChelseaBoots} from './shoes/chelsea-boots';
import {Shoes} from './shoes/shoes';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import { LightService } from './light.service';
import {EXRLoader} from 'three/examples/jsm/loaders/EXRLoader';
import {RGBELoader} from 'three/examples/jsm/loaders/RGBELoader';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit, AfterViewInit {

  @ViewChild('canvas')
  private canvasRef: ElementRef | undefined;

  @Input() public texture = '/assets/texture.jpeg';
  @Input() public cameraZ = 400;
  @Input() public fieldOfView = 35;
  @Input('nearClipping') public nearClippingPlane = 1;
  @Input('farClipping') public farClippingPlane = 1000;

  private camera!: THREE.PerspectiveCamera;
  private renderer!: THREE.WebGLRenderer;
  private scene!: THREE.Scene;
  private envColor = 'skyblue';
  private objectLoaded: any;
  private envMap: any = null;
  private light: any = null;

  public shoes: Shoes[] = [new Richelieu(), new ChelseaBoots()];
  public currentShoes: Shoes | undefined = new Richelieu();
  public parts: any;

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef?.nativeElement;
  }

  constructor(private lightService: LightService) {}

  ngOnInit(): void {
    this.getParts();
  }

  ngAfterViewInit(): void {
    this.createScene();
    this.startRenderingLoop();
  }

  /**
   * Create THREEJS Scene
   * @private
   */
  private createScene(): void {
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color('#f1f1f1');
    this.loadGltf();
    // Camera
    const aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      0.1,
      1000
    );
    this.camera.position.z = this.cameraZ;
    this.camera.position.x = 25;
    this.camera.updateProjectionMatrix();
  //  this.light = this.lightService.directional(this.scene, true);

  /*  const directionalLight = this.lightService.directional(this.scene, true);
    directionalLight.position.set(
      this.camera.position.x,
      this.camera.position.y,
      this.camera.position.z
    );
    //this.scene.add(spotLight);*/
    this.lightService.directional(this.scene, true);


  }

  /**
   * Load external model (format .obj)
   * @private
   */
  public loadObjModel(e: any): void {

    const marronTexture = new THREE.TextureLoader().load( 'assets/models/textures/fabric_leather_02_diff_4k.jpg' );
    marronTexture.wrapS = THREE.MirroredRepeatWrapping;
    marronTexture.wrapT = THREE.RepeatWrapping;

    this.currentShoes = this.shoes.find((shoe: Shoes) => shoe.path === e);
    // Step 1: Load MTL file in order to have default materials and texture
    const mtlLoader = new MTLLoader();
    mtlLoader.load(
      'assets/models/' + e + '.mtl',
      (materials) => {
        materials.preload();
        const objLoader = new OBJLoader();
        objLoader.setMaterials(materials);

        // Step 2: Load OBJ file
        objLoader.load(
          'assets/models/' + e + '.obj',
          (object) => {
            this.scene.remove(this.objectLoaded);
            const box = new THREE.Box3().setFromObject( object );
            const center = new THREE.Vector3();
            box.getCenter( center );
            object.position.sub( center ); // center the model
            this.scene.add(object);
            this.objectLoaded = object;
            this.getParts();
            object.traverse((child) => {
              if (child instanceof THREE.Mesh) {

                child.castShadow = true;
                child.receiveShadow = true;
                if (child.material.map) {
                  child.material.map.anisotropy = 16;
                }
              }
            });

          },
          (xhr) => {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');
          },
          (error) => {
            console.log('An error happened: ' + error);
          }
        );
      }
    );
  }


  public loadGltf(): void {
    const loader = new GLTFLoader();
    loader.load(
      './assets/models/shoes/scene.gltf',
       (gltf) => {

        const model = gltf.scene;
        this.centerModel(model);
        model.traverse((child) => {
          if (child instanceof THREE.Mesh) {
            const material = child.material;

            material.envMap = this.envMap;
          /*  material.map = textureMap;
            material.normalMap = textureNormal;
            material.roughnessMap = textureRough;*/
          }
          console.log(child);
        /*  if (child instanceof THREE.Mesh && child.name === 'front') {
            const material = child.material;
            console.log(material.roughness);
            const texture = new THREE.TextureLoader().load( 'assets/models/richelieu/normal_richelieu.png' );
            texture.wrapS = THREE.MirroredRepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            material.normalMap = texture;
            material.envMap = this.envMap;
            material.roughness  = 1.0;
            console.log(material);
          }*/
        });
        this.scene.add(model);

       // console.log(model.children);


      }
      ,
      // called while loading is progressing
       ( xhr ) => {
        console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
      },
      // called when loading has errors
       ( error ) => {
        console.log( 'An error happened: ' + error );
      }
    );
  }


  /**
   * Get the ration of the canvas
   * @private
   */
  private getAspectRatio(): number {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  /**
   *
   * @private
   */
  private startRenderingLoop(): void {
    this.renderer = new WebGLRenderer({ canvas: this.canvas });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.toneMapping = THREE.ReinhardToneMapping;
    this.renderer.toneMappingExposure = 2.3;
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    const controls = new OrbitControls( this.camera, this.renderer.domElement );
    // controls.update() must be called after any manual changes to the camera's transform
    this.camera.position.set( 50, 20, -45 );
    this.camera.zoom = 100;
    this.camera.updateProjectionMatrix();
    controls.update();

    const component = this;
    (function render(): void  {
      requestAnimationFrame(render);
      component.renderer.render(component.scene, component.camera);
    }());
    this.loadExr();
  }

  /**
   *
   * @param color
   * @param part
   */
  public putColor(color: string, part: string): void {
    this.envColor = color;
    this.objectLoaded.traverse((child: any) => {
      if (child instanceof THREE.Mesh) {
        if (child.name === part) {
          child.material.color = new THREE.Color(this.envColor);
        }
      }
    });
  }

  /**
   *
   */
  public getParts(): void {
    // @ts-ignore
    this.parts = this.currentShoes.getConfigurableParts();
  }

  /**
   *
   * @param model
   * @private
   */
  private centerModel(model: THREE.Object3D): void
  {
    if (model.isObject3D) {
      const box = new THREE.Box3().setFromObject( model );
      const center = new THREE.Vector3();
      box.getCenter( center );
      model.position.sub( center ); // center the model
    }
  }

  private loadExr(): any
  {
    const pmremGenerator = new THREE.PMREMGenerator(this.renderer);

    new EXRLoader()
      .load('assets/models/studio_small_08_4k.exr', (texture) => {
        console.log(pmremGenerator.fromEquirectangular(texture).texture);
        this.envMap = pmremGenerator.fromEquirectangular(texture).texture;
        console.log(this.envMap);
      });


  }


}
