import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccueilComponent} from './accueil/accueil.component';
import {ArticleLayoutComponent} from './shared/article-layout/article-layout.component';
import {ViewerComponent} from './viewer/viewer.component';

const routes: Routes = [
  {path: '', component: AccueilComponent},
  {
    path: 'article', component: ArticleLayoutComponent,
    loadChildren: () => import('./articles/articles.module').then(mod => mod.ArticlesModule)
  },
  { path: '3d-viewer', component: ViewerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VitrineRoutingModule {
}
