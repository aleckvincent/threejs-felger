import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {NgxPageScrollCoreModule} from 'ngx-page-scroll-core';
import {AnimateOnScrollModule} from 'ng2-animate-on-scroll';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CookieService} from 'ngx-cookie-service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AccueilComponent} from './accueil/accueil.component';
import {NotreOffreComponent} from './notre-offre/notre-offre.component';
import {NousContacterComponent} from './nous-contacter/nous-contacter.component';
import {ExperienceComponent} from './experience/experience.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {MenuComponent} from './shared/components/menu/menu.component';
import {CarouselComponent} from './shared/components/carousel/carousel.component';
import {PresseComponent} from './presse/presse.component';
import {ReservationComponent} from './reservation/reservation.component';
import {MentionsLegalesComponent} from './mentions-legales/mentions-legales.component';
import {CookiesComponent} from './shared/components/cookies/cookies.component';
import {SliderComponent} from './shared/components/slider/slider.component';
import {HamburgerComponent} from './shared/components/hamburger/hamburger.component';
import {NewsletterComponent} from './newsletter/newsletter.component';
import {VitrineRoutingModule} from './vitrine-routing.module';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { ArticleLayoutComponent } from './shared/article-layout/article-layout.component';
import {MatCardModule} from '@angular/material/card';
import {NosArticleComponent} from './nos-article/nos-article.component';
import { ViewerComponent } from './viewer/viewer.component';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AccueilComponent,
    NotreOffreComponent,
    NousContacterComponent,
    ExperienceComponent,
    HeaderComponent,
    MenuComponent,
    CarouselComponent,
    PresseComponent,
    ReservationComponent,
    MentionsLegalesComponent,
    CookiesComponent,
    SliderComponent,
    HamburgerComponent,
    NewsletterComponent,
    ArticleLayoutComponent,
    NosArticleComponent,
    ViewerComponent
  ],
    imports: [
        CommonModule,
        VitrineRoutingModule,
        NgxPageScrollCoreModule.forRoot({duration: 1600}),
        AnimateOnScrollModule.forRoot(),
        AngularSvgIconModule.forRoot(),
        MatDatepickerModule,
        MatNativeDateModule,
        MatNativeDateModule,
        MatListModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatExpansionModule,
        NgxUsefulSwiperModule,
        MatSnackBarModule,
        MatCardModule,
        MatSelectModule
    ],
  exports: [
    HeaderComponent,
    CookiesComponent,
    NotreOffreComponent,
    ExperienceComponent,
    PresseComponent,
    NousContacterComponent
  ],
  providers: [
    MatDatepickerModule,
    DatePipe,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    CookieService,
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ]
})
export class VitrineModule {
}
